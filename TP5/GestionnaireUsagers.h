/********************************************
* Titre: Travail pratique #5 - GestionnaireUsagers.h
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard et Robin Kuchue
*******************************************/


#ifndef GESTIONNAIRE_H
#define GESTIONNAIRE_H

#include <set>
#include "Usager.h"
#include "Foncteur.h"
#include "GestionnaireGenerique.h"
#include "Client.h"
#include "ProduitAuxEncheres.h"

using namespace std;

class GestionnaireUsagers:public GestionnaireGenerique<Usager*,set<Usager*>,AjouterUsager,SupprimerUsager>
{
public:
	void afficherProfils() const;
	double obtenirChiffreAffaires() const;	
	void reinitialiser();
	void encherir(Client *client, ProduitAuxEncheres *produit, double montant) const;
};

#endif

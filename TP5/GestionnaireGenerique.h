/********************************************
* Titre: Travail pratique #5 - GestionnaireGenerique.h
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard et Robin Kuchue
*******************************************/

#pragma once

#include <algorithm>

using namespace std;

template<typename T, typename C, typename A, typename S>
class GestionnaireGenerique
{
public:
	C& obtenirConteneur();
	void ajouter(T);
	void supprimer(T);

	template<typename Predicate>
	void pourChaqueElement(Predicate predicat);

protected:
	C conteneur_;
};

/********************************************
*La fonction obtenirConteneur permet de retourner le conteneur du gestionnaire
*
*return : le conteneur du type C
*******************************************/
template <typename T, typename C, typename A, typename S>
C& GestionnaireGenerique<T, C, A, S>::obtenirConteneur() {
	return conteneur_;
}

/********************************************
*La fonction ajouter ajoute un element dans la liste du gestionnaire
*
*param t : le pointeur vers l'�l�ment
*******************************************/
template <typename T, typename C, typename A, typename S>
void GestionnaireGenerique<T, C, A, S>::ajouter(T t) {
	A a(conteneur_);
	a(t);
}

/********************************************
*La fonction supprimer enleve un element dans la liste du gestionnaire
*
*param t : le pointeur vers l'�l�ment
*******************************************/
template <typename T, typename C, typename A, typename S>
void GestionnaireGenerique<T, C, A, S>::supprimer(T t) {
	S s(conteneur_);
	s(t);
}

/********************************************
*La fonction pourChaqueElement applique un foncteur passer en param�tre � chaque �l�ment du conteneur 
*
*param predicat : le foncteur
*******************************************/
template <typename T, typename C, typename A, typename S>
template <typename Predicate>
void GestionnaireGenerique<T, C, A, S>::pourChaqueElement(Predicate predicat) {
	for_each(conteneur_.begin(), conteneur_.end(), predicat);
}
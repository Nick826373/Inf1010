/********************************************
* Titre: Travail pratique #5 - GestionnaireProduits.cpp
* Date: 9 mars 2018
* Auteur: Ryan Hardie
*******************************************/

#include "GestionnaireProduits.h"

/********************************************
*La fonction reinitialiserClient permet de vider la liste de produit d'un client
*******************************************/
void GestionnaireProduits::reinitialiserClient()
{
	conteneur_.clear();
}

/********************************************
*La fonction reinitialiserClient permet de vider la liste de produit d'un fournisseur en prenant soin d'enlever la r�f�rence au fournisseur dans le produit
*******************************************/
void GestionnaireProduits::reinitialiserFournisseur()
{
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++) {
		(it->second)->modifierFournisseur(nullptr);
	}
	conteneur_.clear();
}

/********************************************
*La fonction afficher permet d'afficher le panier ou catalogue des usagers facilement dans la console avec un cout
*en appelant la fonction affciher de chaque produit et en comptant.
*******************************************/
void GestionnaireProduits::afficher()
{
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++) {
		(it->second)->afficher();
		auto compteRange = conteneur_.equal_range(it->first);//donne l'iterateur de d�but et de fin dans un pair de tout les �l�ment ayant la meme key
		int compte = distance(compteRange.first, compteRange.second);//donne le nombre d'�l�ment entre les it�rateurs
		cout << "\t\texemplaires : "<< compte << endl;
		for (int i = 1; i < compte; i++)
		{
			it++;
		}
	}		
}

/********************************************
*La fonction obtenirTotalAPayer retourne la somme des prix du panier ou catalogue
*
*return : Retourne la somme des prix
*******************************************/
double GestionnaireProduits::obtenirTotalAPayer()
{
	double totalAPayer = 0;
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++)
		totalAPayer += (it->second)->obtenirPrix();
	return totalAPayer;
}

/********************************************
*La fonction obtenirTotalAPayer retourne la somme des prix du panier en tenant compte du rabais premium
*
*return : Retourne la somme des prix
*******************************************/
double GestionnaireProduits::obtenirTotalApayerPremium()
{
	double montant = 0;
	for (auto it = conteneur_.begin(); it != conteneur_.end(); it++){
		double prix =it->second->obtenirPrix();
		montant += prix < 5 ? 0 : prix - 5;
	}
	return montant;
}

/********************************************
*La fonction trouverProduitPlusCher permet de retourner le produit le plus cher du panier
*
*return : un pointeur vers le produit le plus cher
*******************************************/
Produit * GestionnaireProduits::trouverProduitPlusCher()
{
	auto comparerPrix = [](pair<int,Produit*> prod1, pair<int, Produit*> prod2)->bool {return prod1.second->obtenirPrix() < prod2.second->obtenirPrix(); };
	auto it = max_element(conteneur_.begin(), conteneur_.end(), comparerPrix);
	if (it != conteneur_.end())
		return it->second;
}

/********************************************
*La fonction obtenirProduitsEntre permet de trouver les produits dont le prix est situer entre deux bornes
*
*param borneInf: la borne inf�rieur
*param borneSup: la borne sup�rieur
*return : un vecteur de pair <cl�,produit> contenant les produits contenu dans l'interval
*******************************************/
vector<pair<int, Produit*>> GestionnaireProduits::obtenirProduitsEntre(double borneInf, double borneSup)
{
	vector<pair<int, Produit*>> vecteur;
	copy_if(conteneur_.begin(), conteneur_.end(),back_inserter(vecteur), Intervalle(borneInf,borneSup));
	return vecteur;
}

/********************************************
*La fonction obtenirProduitSuivant permet de trouver le produit ayant la cl� sup�rieur au produit passer en param�tre la plus proche num�riquement 
*
*param borneInf: la borne inf�rieur
*param borneSup: la borne sup�rieur
*return : un vecteur de pair <cl�,produit> contenant les produits contenu dans l'interval
*******************************************/
Produit * GestionnaireProduits::obtenirProduitSuivant(Produit * produit)
{
	auto it = find_if(conteneur_.begin(),conteneur_.end(),bind(greater<pair<int,Produit*>>(),_1,make_pair(produit->obtenirReference(),produit)));
	return it->second;
}


/********************************************
* Titre: Travail pratique #5 - Foncteur.h
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard et Robin Kuchue
*******************************************/

#pragma once
#include <cstdlib>
#include <map>
#include <set>
#include <algorithm>
#include "Produit.h"
#include "Usager.h"

using namespace std;

/********************************************
*Le foncteur Egal prend un objet d'un type T � la construction et retourne vrai ensuite si le deuxieme �l�ment de la pair passer entre parenth�se
*est identique � l'objet passer � la construction
*******************************************/
template<typename T>
class Egal {
public:
	Egal(T t):
		t_(t) {};
	bool operator()(const pair<int,T> &paire) {
		return t_ == paire.second;
	};
private:
	T t_;
};

/********************************************
*Le foncteur FoncteurGenerateurId retourne un int incr�menter � chaque fois que l'on l'appel
*******************************************/
class FoncteurGenerateurId {
public:
	FoncteurGenerateurId()
		:id_(0) {};
	int operator()(){
		return id_++;
	};
private:
	int id_;
};


/********************************************
*Le foncteur DiminuerPourcent diminue le prix d'un produit d'une pair passer entre parenth�se par le pourcentage passer au constructeur
*******************************************/
class DiminuerPourcent {
public:
	DiminuerPourcent(int pourcentage)
		:pourcentage_(pourcentage) {
	};
	void operator()(pair<int, Produit*> paire) {
		paire.second->modifierPrix(paire.second->Produit::obtenirPrix()*((100 - pourcentage_)/100.0));
	};
private:
	int pourcentage_;
};

/********************************************
*Le foncteur Intervalle retourne vrai si le prix d'un produit d'une pair passer entre parenth�se est entre les deux bornes passer au constructeur
*******************************************/
class Intervalle{
public:
	Intervalle(double borneInf, double borneSup)
		:borneInf_(borneInf), borneSup_(borneSup) {};
	bool operator()(pair<int, Produit*> paire) {
		return (paire.second->obtenirPrix() >= borneInf_ && paire.second->obtenirPrix() <= borneSup_);
	};
private:
	double borneInf_;
	double borneSup_;

};

/********************************************
*Le foncteur AjouterProduit ajoute un produit passer entre parenth�se � une multimap passer au constructeur
*******************************************/
class AjouterProduit {
public:
	AjouterProduit(multimap<int, Produit*> &multimap)
		:multimap_(multimap) {};
	multimap<int, Produit*>& operator()(Produit* produit) {
		multimap_.insert(make_pair(produit->obtenirReference(), produit));	
		return multimap_;
	};
private:
	multimap<int, Produit*>& multimap_;
};

/********************************************
*Le foncteur SupprimerProduit supprime un produit passer entre parenth�se � une multimap passer au constructeur
*******************************************/
class SupprimerProduit {
public:
	SupprimerProduit(multimap<int, Produit*> &multimap)
		:multimap_(multimap) {};
	 multimap<int, Produit*> operator()(Produit* produit) {
		Egal<Produit*> egalP(produit);
		auto it = find_if(multimap_.begin(), multimap_.end(), egalP);
		if(it != multimap_.end())
			multimap_.erase(it);
		return multimap_;
	};
private:
	multimap<int, Produit*> &multimap_;
};

/********************************************
*Le foncteur AjouterProduit ajoute un usager passer entre parenth�se � un set passer au constructeur
*******************************************/
class AjouterUsager {
public:
	AjouterUsager(set<Usager*> &set)
		:set_(set) {};
	set<Usager*>& operator()(Usager* usager) {
		set_.insert(usager);
		return set_;
	};
private:
	set<Usager*>& set_;
};

/********************************************
*Le foncteur SupprimerUsager supprime un usager passer entre parenth�se � un set passer au constructeur
*******************************************/
class SupprimerUsager {
public:
	SupprimerUsager(set<Usager*> &set)
		:set_(set) {};
	set<Usager*> operator()(Usager* usager) {
		auto it=set_.begin();
		for (; it != set_.end(); it++)
		{
			if (usager->obtenirReference() == (*it)->obtenirReference())
				set_.erase(it);
				break;
		}		
	};
private:
	set<Usager*> &set_;
};

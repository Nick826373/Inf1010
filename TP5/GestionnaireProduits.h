/********************************************
* Titre: Travail pratique #5 - GestionnaireProduits.h
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard et Robin Kuchue
*******************************************/

#ifndef GESTIONNAIREPRODUITS_H
#define GESTIONNAIREPRODUITS_H

#include <map>
#include <algorithm>
#include <functional>
#include <vector>
#include <iostream>
#include "Produit.h"
#include "Foncteur.h"
#include "GestionnaireGenerique.h"

using namespace std;
using namespace std::placeholders;

class GestionnaireProduits :public GestionnaireGenerique<Produit*, multimap<int,Produit*>, AjouterProduit, SupprimerProduit>
{
public:
	void reinitialiserClient();
	void reinitialiserFournisseur();
	void afficher();
	double obtenirTotalAPayer();
	double obtenirTotalApayerPremium();
	Produit* trouverProduitPlusCher();
	vector<pair<int,Produit*>> obtenirProduitsEntre(double borneInf, double borneSup);
	Produit* obtenirProduitSuivant(Produit*);
};

#endif
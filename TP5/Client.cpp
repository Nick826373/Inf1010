/********************************************
* Titre: Travail pratique #5 - Client.cpp
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Client.h"
#include "ProduitAuxEncheres.h"
#include <iostream>

/********************************************
*Le premier constructeur par param�tres de la classe ClientPremium initialise les attributs h�riter d'usager � leur
*valeur par default et le codeClient passer � la valeur passer en param�tre
*******************************************/
Client::Client(unsigned int codeClient)
    : Usager(),
      codeClient_(codeClient)
{
	panier_ = new GestionnaireProduits();
}

/********************************************
*Le constructeur par param�tres de la Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Client::Client(const string &nom, const string &prenom, int identifiant,
               const string &codePostal, unsigned int codeClient)
    : Usager(nom, prenom, identifiant, codePostal),
      codeClient_(codeClient)
{
	panier_ = new GestionnaireProduits();
}

/********************************************
*La fonction obtenirCodeClient retourne la valeur du parametre codeClient_
*
*return : Retourne l'code du client.
*******************************************/
Client::~Client() {
	delete panier_;
}

/********************************************
*La fonction obtenirCodeClient retourne la valeur du parametre codeClient_
*
*return : Retourne l'code du client.
*******************************************/
unsigned int Client::obtenirCodeClient() const
{
    return codeClient_;
}

/********************************************
*La fonction obtenirPanier retourne la valeur du parametre Panier_
*
*return : Retourne le vecteur de pointeur des produits du panier.
*******************************************/
GestionnaireProduits* Client::obtenirPanier() const
{
	// TODO : � modifier
    return panier_;
}

/********************************************
*La fonction obtenirTotalApayer retourne le total du prix des items du client.
*
*return : Retourne la somme des prix des objets du panier.
*******************************************/
double Client::obtenirTotalAPayer() const
{
	// TODO : � modifier
	return panier_->obtenirTotalAPayer();
}

/********************************************
*La fonction afficherPanier permet d'afficher le contenu du panier d'un client facilement dans la console.
*******************************************/
void Client::afficherPanier() const
{
	// TODO : � modifier
    cout << "PANIER (de " << obtenirNom() << ")"
         << "\n";
	panier_->afficher();
    cout << endl;
}

/********************************************
*La fonction afficher permet d'afficher le profil(attribut autre que le panier) d'un client facilement dans la console.
*******************************************/
void Client::afficher() const
{
	// TODO : � modifier
    Usager::afficher();
    cout << "\t\tcode client:\t" << codeClient_ << endl
         << "\t\tpanier:\t\t" << panier_->obtenirConteneur().size() << " elements" << endl;
}

/********************************************
*La fonction modifierCodeClient modifie le parametre codeClient_
*
*param codeClient : Nouveaux codeClient_.
*******************************************/
void Client::modifierCodeClient(unsigned int codeClient)
{
    codeClient_ = codeClient_;
}

/********************************************
*La fonction enleverProduit enleve un produit du panier du client.
*
*param produit : le pointeur vers le produit � enlever.
*******************************************/
void Client::enleverProduit(Produit *produit)
{
	// TODO : � modifier
	panier_->supprimer(produit);
}

/********************************************
*La fonction ajouterProduit ajoute un produit au panier du client.
*
*param produit : le pointeur vers le produit � ajouter.
*******************************************/
void Client::ajouterProduit(Produit *produit)
{
	// TODO : � modifier
	panier_->ajouter(produit);
}

/********************************************
*La fonction r�initialiser permet de vider le panier d'un client
*******************************************/
void Client::reinitialiser()
{
	// TODO : � modifier
	multimap<int, Produit*>::iterator it;
	for (it = panier_->obtenirConteneur().begin(); it != panier_->obtenirConteneur().end(); it++)
	{
		ProduitAuxEncheres *produit = dynamic_cast<ProduitAuxEncheres *>(it->second);
		if (produit) {
			produit->modifierEncherisseur(nullptr);
			produit->modifierPrix(produit->obtenirPrixInitial());
		}
	}
	panier_->reinitialiserClient();
}

/********************************************
*La fonction trouverProduitPlusCher retourne appel la m�thode �quivalente du panier
*
*return : Retourne un pointeur vers le produit le plus cher du catalogue_.
*******************************************/
Produit *Client::trouverProduitPlusCher() const{
	return panier_->trouverProduitPlusCher();
}

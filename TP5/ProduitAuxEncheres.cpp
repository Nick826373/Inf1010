/********************************************
* Titre: Travail pratique #4 - ProduitAuxEncheres.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "ProduitAuxEncheres.h"

/********************************************
*Le premier constructeur par param�tres de la classe ProduitAuxEncheres initialise les attributs h�riter de produit � leur
*valeur par default et le prix initial � la valeur passer passer en param�tre. L'encherisseur est un pointeur null par default
*******************************************/
ProduitAuxEncheres::ProduitAuxEncheres(double prix)
    : Produit(),
      prixInitial_(prix),
      encherisseur_(nullptr)
{
}

/********************************************
*Le deuxieme constructeur par param�tres de la ProduitAuxEncheres initialise les attributs aux valeurs passer en param�tre et l'encherisseur est un pointeur null.
*******************************************/
ProduitAuxEncheres::ProduitAuxEncheres(Fournisseur *fournisseur, const string &nom,
                                       int reference, double prix)
    : Produit(fournisseur, nom, reference, prix),
      prixInitial_(prix),
      encherisseur_(nullptr)
{
}

/********************************************
*La fonction obtenirPrixInitial retourne la valeur du parametre prixInitial_
*
*return : Retourne la mise minimum pouvant etre fait sur le produit.
*******************************************/
double ProduitAuxEncheres::obtenirPrixInitial() const
{
    return prixInitial_;
}

/********************************************
*La fonction obtenirEncherisseur retourne la valeur du parametre encherisseur_
*
*return : Retourne un pointeur vers le client ayant fait la plus haute mise.
*******************************************/
Client *ProduitAuxEncheres::obtenirEncherisseur() const
{
    return encherisseur_;
}

/********************************************
*La fonction afficher permet d'afficher les attributs d'un produitAuxEncheres facilement dans la console.
*******************************************/
void ProduitAuxEncheres::afficher() const
{
    Produit::afficher();
    cout << "\t\tprix initial:\t" << prixInitial_ << endl
         << "\t\tencherisseur:\t" << encherisseur_->obtenirNom() << endl;
}

/********************************************
*La fonction modifierPrixInitial modifie le parametre prixInitial_
*
*param prixInitial : prixInitial_.
*******************************************/
void ProduitAuxEncheres::modifierPrixInitial(double prixInitial)
{
    prixInitial_ = prixInitial;
}

/********************************************
*La fonction modifierPrixInitial modifie le parametre encherisseur_
*
*param encherisseur : encherisseur_.
*******************************************/
void ProduitAuxEncheres::modifierEncherisseur(Client *encherisseur)
{
    encherisseur_ = encherisseur;
}

/********************************************
*La fonction mettreAJourEnchere modifie le param�tre encherisseur_ et prix_ � condition que la mise soit valide.
*La mise est valide si le montant est sup�rieur au pr�c�dant et si le client misant sur le produit n'a pas deja la plus haute mise.
*
*param prixInitial : prixInitial_.
*******************************************/
void ProduitAuxEncheres::mettreAJourEnchere(Client *encherisseur, double nouveauPrix)
{
    if (encherisseur_ == encherisseur)
        return;
    prix_ = nouveauPrix;
    encherisseur->ajouterProduit(this);
    if (encherisseur_ != nullptr)
        encherisseur_->enleverProduit(this);
    encherisseur_ = encherisseur;
}

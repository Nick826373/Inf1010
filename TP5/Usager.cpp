/********************************************
* Titre: Travail pratique #5 - Usager.cpp
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Usager.h"
#include <iostream>

/********************************************
*Le constructeur par param�tres de la classe Usager initialise les attributs de l'usager au valeur passer en param�tre
*******************************************/
Usager::Usager(const string &nom, const string &prenom, int reference,
	const string &codePostal)
	: nom_(nom),
	prenom_(prenom),
	reference_(reference),
	codePostal_(codePostal)
{
}

/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom de l'usager.
*******************************************/
string Usager::obtenirNom() const
{
	return nom_;
}

/********************************************
*La fonction obtenirPrenom retourne la valeur du parametre prenom_
*
*return : Retourne le prenom de l'usager.
*******************************************/
string Usager::obtenirPrenom() const
{
	return prenom_;
}

/********************************************
*La fonction obtenirIdentifiant retourne la valeur du parametre reference_
*
*return : Retourne la reference de l'usager.
*******************************************/
int Usager::obtenirReference() const
{
    return reference_;
}

/********************************************
*La fonction obtenirCodePostal retourne la valeur du parametre codePostal_
*
*return : Retourne le code postale de l'usager.
*******************************************/
string Usager::obtenirCodePostal() const
{
	return codePostal_;
}

/********************************************
*La fonction obtenirTotalApayer retourne 0. La fonction est d�finie seulement pour les classes enfants qui ne red�finisse pas cette fontion ne change pas les sommes de totaux � payer.
*
*return : 0.
*******************************************/
double Usager::obtenirTotalAPayer() const
{
	return 0;
}

/********************************************
*La fonction afficher permet d'afficher le profil(attribut) d'un usager facilement dans la console.
*******************************************/
void Usager::afficher() const
{
    cout << "\t" << nom_ << ", " << prenom_ << " (" << reference_ << ")" << endl
        << "\t\tcode postal:\t" << codePostal_ << endl;
}

/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void Usager::modifierNom(const string &nom)
{
	nom_ = nom;
}

/********************************************
*La fonction modifierPrenom modifie le parametre prenom_
*
*param prenom : Nouveaux prenom_.
*******************************************/
void Usager::modifierPrenom(const string &prenom)
{
	prenom_ = prenom;
}

/********************************************
*La fonction modifierIdentifiant modifie le parametre reference_
*
*param reference : Nouveaux reference_.
*******************************************/
void Usager::modifierReference(int reference)
{
	reference_ = reference;
}

/********************************************
*La fonction modifierCodePostal modifie le parametre codePostal_
*
*param codePostal : Nouveaux codePostal_.
*******************************************/
void Usager::modifierCodePostal(const string &codePostal)
{
    codePostal_ = codePostal;
}

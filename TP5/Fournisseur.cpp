/********************************************
* Titre: Travail pratique #5 - Fournisseur.cpp
* Date: 9 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Fournisseur.h"
#include <iostream>

/********************************************
*Le constructeur par default de la classe Fournisseur utilise le constructeur par default de la classe Usager
*******************************************/
Fournisseur::Fournisseur()
    : Usager()
{
	catalogue_ = new GestionnaireProduits();
}

/********************************************
*Le constructeur par param�tres de la classe Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Fournisseur::Fournisseur(const string &nom, const string &prenom, int identifiant,
                         const string &codePostal)
    : Usager(nom, prenom, identifiant, codePostal)
{
	catalogue_ = new GestionnaireProduits();
}

/********************************************
*Le destructeur de la classe Fournisseur detruit les attributs allou� dynamiquement
*******************************************/
Fournisseur::~Fournisseur() {
	delete catalogue_;
}

/********************************************
*La fonction obtenirCatalogue retourne la valeur du parametre contenuCatalogue_
*
*return : Retourne le vecteur contenant les produits du fournisseur.
*******************************************/
GestionnaireProduits* Fournisseur::obtenirCatalogue() const
{
	// TODO : � modifier
    return catalogue_;
}

/********************************************
*La fonction afficherCatalogue permet d'afficher le contenu du catalogue d'un fournisseur facilement dans la console.
*******************************************/
void Fournisseur::afficherCatalogue() const
{
	// TODO : � modifier
    cout << "CATALOGUE (de " << obtenirNom() << ")"
         << "\n";
	catalogue_->afficher();
	cout << endl;
}

/********************************************
*La fonction afficher permet d'afficher le profil(attribut autre que le catalogue) d'un fournisseur facilement dans la console.
*******************************************/
void Fournisseur::afficher() const
{
	// TODO : � modifier
    Usager::afficher();
    cout << "\t\tcatalogue:\t" << catalogue_->obtenirConteneur().size() << " elements" << endl;
}

/********************************************
*La fonction r�initialiser permet de vider le catalogue d'un fournisseur et de remettre � la valeur par default l'attribut fournisseur d'un produit.
*******************************************/
void Fournisseur::reinitialiser()
{
	// TODO : � modifier
	catalogue_->reinitialiserFournisseur();
}

/********************************************
*La fonction ajouterProduit ajoute un produit dans le catatogue du fournisseur.
*
*param produit : le pointeur vers le produit � ajouter.
*******************************************/
void Fournisseur::ajouterProduit(Produit *produit)
{
	// TODO : � modifier
	if(produit->obtenirFournisseur()!=nullptr)
		produit->obtenirFournisseur()->enleverProduit(produit);
	produit->modifierFournisseur(this);
	catalogue_->ajouter(produit);
}

/********************************************
*La fonction enleverProduit enleve un produit du catatogue du fournisseur
*
*param produit : le pointeur vers le produit � enlever.
*******************************************/
void Fournisseur::enleverProduit(Produit *produit)
{
	// TODO : � modifier
	catalogue_->supprimer(produit);
	produit->modifierFournisseur(nullptr);
}

/********************************************
*La fonction trouverProduitPlusCher retourne appel la m�thode �quivalente du catalogue
*
*return : Retourne un pointeur vers le produit le plus cher du catalogue_.
*******************************************/
Produit * Fournisseur::trouverProduitPlusCher() const
{
	return catalogue_->trouverProduitPlusCher();
}

/********************************************
*La fonction DiminuerPrix diminue le prix de tout les produits du catalogue � l'aide de la m�thode �quivalente du catalogue
*
*param pourcent : le pourcentage du prix a enlever
*******************************************/
void Fournisseur::DiminuerPrix(int pourcent) const
{
	catalogue_->pourChaqueElement(DiminuerPourcent(pourcent));
}

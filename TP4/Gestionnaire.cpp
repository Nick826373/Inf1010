/********************************************
* Titre: Travail pratique #4 - Gestionnaire.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Gestionnaire.h"

/********************************************
*R�ponse aux questions
*
*Q1:1
*
*Q2:La m�thode f() n'est pas d�clarer virtuelle dans la classe A. Puisque la classe A est la classe parente et
*que le vecteur contient un pointeur vers un objet A, c'est sa m�thode qui doit absolument etre d�clarer virtuelle
*pour que le polymorphisme fonctionne
*
*Q3:Premi�rement on peut effacer le mot virtual devant la d�claration de la m�thode f() de la classe B.
*Deuxi�ment, on peut effacer la red�finition de la m�thode f() dans la classe C.
*
*Q4:La classe usager contient des m�thode virtuelle pure qui ne sont pas d�finie seulement d�clarer pour les classe enfants.
*La classe est donc abstraite et ne peut pas etre instancier.
*
*Q5:On utilise un vecteur diff�rent pour chaque les produits et un autre pour les produits sold�s
*
*******************************************/

/********************************************
*La fonction obtenirUsagers retourne la valeur du parametre usagers_
*
*return : Retourne un vecteur contenant des pointeurs vers chaque usager du client.
*******************************************/
vector<Usager *> Gestionnaire::obtenirUsagers() const
{
    return usagers_;
}

/********************************************
*La fonction afficherLesProfils permet d'afficher les profils des usagers facilement dans la console avec un cout
*en appelant la fonction affciherProfils de chaque usager.
*******************************************/
void Gestionnaire::afficherLesProfils() const
{
	cout << "PROFILS" << endl;
	for (unsigned i = 0; i < usagers_.size(); i++){
		usagers_[i]->afficherProfil();
	}
}

/********************************************
*La fonction obtenirChiffreAffaires permet de retourner le total de chaque panier de chaque client.
*
*return : le total de tous les produits de tous les clients
*******************************************/
double Gestionnaire::obtenirChiffreAffaires() const
{
	double total = 0;
	for (unsigned i = 0; i < usagers_.size(); i++)
	{
		total +=usagers_[i]->obtenirTotalAPayer();
	}
	return total;
}

/********************************************
*La fonction ajouterUsager ajoute un usager dans la liste du gestionnaire
*
*param usager : le pointeur vers l'usager � ajouter.
*******************************************/
void Gestionnaire::ajouterUsager(Usager *usager)
{
	//v�rifie si l'usager est deja dans la liste
	bool pasDansListe = true;
	for (unsigned i = 0; i < usagers_.size(); i++){
		if (usagers_[i] == usager)
			pasDansListe = false;
	}
	//ajoute l'usager s'il n'est pas dans la liste
	if (pasDansListe)
		usagers_.push_back(usager);
}

/********************************************
*La fonction r�initialiser permet de vider la liste d'usager d'un gestionnaire
*******************************************/
void Gestionnaire::reinitialiser()
{
	for (unsigned i = 0; i < usagers_.size(); i++) {
		usagers_[i]->reinitialiser();
	}
}

/********************************************
*La fonction encherir permet de changer le prix et le client qui poss�de un produit au ench�re si le nouveau prix est sup�rieur a l'ancien
*
*param client: le client misant sur l'ench�re
*param produit: le ProduitAuxEnchere sur lequel le client a miser
*param prix: le montant miser
*******************************************/
void Gestionnaire::encherir(Client *client, ProduitAuxEncheres *produit, double montant) const
{
	if (produit->obtenirPrix() < montant)
		produit->mettreAJourEnchere(client, montant);
}

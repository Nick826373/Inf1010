/********************************************
* Titre: Travail pratique #4 - Fournisseur.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Fournisseur.h"
#include <iostream>

/********************************************
*Le constructeur par default de la classe Fournisseur utilise le constructeur par default de la classe Usager
*******************************************/
Fournisseur::Fournisseur()
    : Usager()
{
}

/********************************************
*Le constructeur par param�tres de la classe Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Fournisseur::Fournisseur(const string &nom, const string &prenom, int identifiant, const string &codePostal)
    : Usager(nom, prenom, identifiant, codePostal)
{
}

/********************************************
*La fonction obtenirCatalogue retourne la valeur du parametre contenuCatalogue_
*
*return : Retourne le vecteur contenant les produits du fournisseur.
*******************************************/
vector<Produit *> Fournisseur::obtenirCatalogue() const
{
    return catalogue_;
}

/********************************************
*La fonction afficherCatalogue permet d'afficher le contenu du catalogue d'un fournisseur facilement dans la console.
*******************************************/
void Fournisseur::afficherCatalogue() const
{
    cout << "CATALOGUE (de " << obtenirNom() << ")"
         << "\n";
    for (unsigned int i = 0; i < catalogue_.size(); i++)
        catalogue_[i]->afficher();
    cout << endl;
}

/********************************************
*La fonction afficherProfil permet d'afficher le profil(attribut autre que le catalogue) d'un fournisseur facilement dans la console.
*******************************************/
void Fournisseur::afficherProfil() const
{
	Usager::afficherProfil();
	cout << "\t\tcatalogue:\t" << catalogue_.size() << " elements"<<endl;
}

/********************************************
*La fonction r�initialiser permet de vider le catalogue d'un fournisseur et de remettre � la valeur par default l'attribut fournisseur d'un produit.
*******************************************/
void Fournisseur::reinitialiser()
{
	for (unsigned i = 0; i < catalogue_.size(); i++){
		catalogue_[i]->modifierFournisseur(nullptr);
	}
	catalogue_.clear();
}

/********************************************
*La fonction ajouterProduit ajoute un produit dans le catatogue du fournisseur.
*
*param produit : le pointeur vers le produit � ajouter.
*******************************************/
void Fournisseur::ajouterProduit(Produit *produit)
{
	//v�rifie que le produit ne soit pas deja dans le catalogue
    for (unsigned int i = 0; i < catalogue_.size(); i++)
        if (catalogue_[i] == produit)
            return;
	//enleve le produit du catalogue du fournisseur pr�c�dant si celui-ci existe
    Fournisseur *fournisseur = produit->obtenirFournisseur();
    if (fournisseur != nullptr && fournisseur != this)
        fournisseur->enleverProduit(produit);
	//ajoute le produit au catalogue
    catalogue_.push_back(produit);
}

/********************************************
*La fonction enleverProduit enleve un produit du catatogue du fournisseur
*
*param produit : le pointeur vers le produit � enlever.
*******************************************/
void Fournisseur::enleverProduit(Produit *produit)
{
	//remet null l'attribut fournisseur du produit
    produit->modifierFournisseur(nullptr);
	//parcours le panier pour trouver le produit
    for (unsigned int i = 0; i < catalogue_.size(); i++)
    {
        if (catalogue_[i] == produit)
        {
			//supprime le produit
            catalogue_[i] = catalogue_[catalogue_.size() - 1];
            catalogue_.pop_back();
            return;
        }
    }
}

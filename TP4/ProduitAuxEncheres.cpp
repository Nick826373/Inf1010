/********************************************
* Titre: Travail pratique #4 - ProduitAuxEncheres.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "ProduitAuxEncheres.h"

/********************************************
*Le premier constructeur par param�tres de la classe ProduitAuxEncheres initialise les attributs h�riter de produit � leur
*valeur par default et le prix initial � la valeur passer passer en param�tre. L'encherisseur est un pointeur null par default
*******************************************/
ProduitAuxEncheres::ProduitAuxEncheres(double prix)
    : Produit(),
      prixInitial_(prix),
      encherisseur_(nullptr)
{
}

/********************************************
*Le deuxieme constructeur par param�tres de la ProduitAuxEncheres initialise les attributs aux valeurs passer en param�tre et l'encherisseur est un pointeur null.
*******************************************/
ProduitAuxEncheres::ProduitAuxEncheres(Fournisseur *fournisseur, const string &nom,
                                       int reference, double prix)
    : Produit(fournisseur, nom, reference, prix),
      prixInitial_(prix),
      encherisseur_(nullptr)
{
}

/********************************************
*La fonction obtenirPrixInitial retourne la valeur du parametre prixInitial_
*
*return : Retourne la mise minimum pouvant etre fait sur le produit.
*******************************************/
double ProduitAuxEncheres::obtenirPrixInitial() const
{
    return prixInitial_;
}

/********************************************
*La fonction obtenirEncherisseur retourne la valeur du parametre encherisseur_
*
*return : Retourne un pointeur vers le client ayant fait la plus haute mise.
*******************************************/
Client *ProduitAuxEncheres::obtenirEncherisseur() const
{
    return encherisseur_;
}

/********************************************
*La fonction afficher permet d'afficher les attributs d'un produitAuxEncheres facilement dans la console.
*******************************************/
void ProduitAuxEncheres::afficher() const
{
	Produit::afficher();
	cout << "\t\tprixInitial: $" << prixInitial_ << ""<<endl
		<< "\t\tencherisseur: " << encherisseur_->obtenirNom()<<endl;//afficher le nom pas lobjet
}

/********************************************
*La fonction modifierPrixInitial modifie le parametre prixInitial_
*
*param prixInitial : prixInitial_.
*******************************************/
void ProduitAuxEncheres::modifierPrixInitial(double prixInitial)
{
    prixInitial_ = prixInitial;
}

/********************************************
*La fonction mettreAJourEnchere modifie le param�tre encherisseur_ et prix_ � condition que la mise soit valide.
*La mise est valide si le montant est sup�rieur au pr�c�dant et si le client misant sur le produit n'a pas deja la plus haute mise.
*
*param prixInitial : prixInitial_.
*******************************************/
void ProduitAuxEncheres::mettreAJourEnchere(Client *encherisseur, double nouveauPrix)
{
	//v�rifie que le client ne poss�de pas deja le produit dans son panier
	if (encherisseur_!=encherisseur) {

		//mise � jour du prix
		prix_ = nouveauPrix;

		//v�rifie qu'on passe un vrai client en param�tre avant d'ajouter le produit � son panier
		if(encherisseur!=nullptr)
			encherisseur->ajouterProduit(this);

		//v�rifie si l'ancien encherisseur existe avant d'enlever le produit de son panier
		if(encherisseur_!=NULL)
			encherisseur_->enleverProduit(this);

		//met � jour l'encherisseur
		encherisseur_ = encherisseur;
	}
}

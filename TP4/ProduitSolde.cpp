/********************************************
* Titre: Travail pratique #4 - ProduitSolde.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "ProduitSolde.h"

/********************************************
*Le premier constructeur par param�tres de la classe ProduitSolde initialise les attributs h�riter de produit � leur
*valeur par default et le pourcentage de rabais � la valeur passer passer en param�tre
*******************************************/
ProduitSolde::ProduitSolde(int pourcentageRabais)
    : Produit(),
      Solde(pourcentageRabais)
{
}

/********************************************
*Le deuxieme constructeur par param�tres de la classe ProduitSolde initialise les attributs aux valeurs passer en param�tre
*******************************************/
ProduitSolde::ProduitSolde(Fournisseur *fournisseur, const string &nom,
                           int reference, double prix, int pourcentageRabais)
    : Produit(fournisseur, nom, reference, prix),
      Solde(pourcentageRabais)
{
}

/********************************************
*La fonction obtenirPrix retourne la valeur du parametre prix_ moins le rabais en vigueur sur le produit
*
*return : Retourne le prix du produit.
*******************************************/
double ProduitSolde::obtenirPrix() const
{
	return prix_- (prix_*pourcentageRabais_ / 100);
}

/********************************************
*La fonction afficher permet d'afficher les attributs d'un produit facilement dans la console.
*******************************************/
void ProduitSolde::afficher() const
{
	Produit::afficher();
	cout << "\t\trabais: " << pourcentageRabais_<< "%" <<endl;
}

/********************************************
* Titre: Travail pratique #4 - ClientPremium.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "ClientPremium.h"
#include <iostream>

/********************************************
*Le premier constructeur par param�tres de la classe ClientPremium initialise les attributs h�riter d'usager � leur
*valeur par default et le nombre de jour restant � la valeur passer passer en param�tre
*******************************************/
ClientPremium::ClientPremium(unsigned int joursRestants)
    : Client(),
      joursRestants_(joursRestants)
{
}

/********************************************
*Le deuxieme constructeur par param�tres de la ClientPremium initialise les attributs aux valeurs passer en param�tre
*******************************************/
ClientPremium::ClientPremium(const string &nom, const string &prenom, int identifiant,
                             const string &codePostal, unsigned int codeClient,
                             unsigned int joursRestants)
    : Client(nom, prenom, identifiant, codePostal, codeClient),
      joursRestants_(joursRestants)
{
}

/********************************************
*La fonction obtenirJoursRestants retourne la valeur du parametre joursRestants_
*
*return : Retourne le nombre de jour restant au status premium du client
*******************************************/
unsigned int ClientPremium::obtenirJoursRestants() const
{
    return joursRestants_;
}

/********************************************
*La fonction obtenirTotalApayer retourne le total du prix des items du client en prenant en compte sont rabais premium
*
*return : Retourne la somme des prix des objets du panier incluant le rabais premium
*******************************************/
double ClientPremium::obtenirTotalAPayer() const
{
	double total = 0;

	for (unsigned i = 0; i < panier_.size(); i++){
		if(panier_[i]->obtenirPrix()>RABAIS)//ajout de condition sinon un item valant moins de 5 reduit le total des autres items
			total += panier_[i]->obtenirPrix() - RABAIS;
	}
	return total;
}

/********************************************
*La fonction afficherProfil permet d'afficher le profil(attribut autre que le catalogue) d'un ClietnPremium facilement dans la console avec un cout.
*******************************************/
void ClientPremium::afficherProfil() const
{
	Client::afficherProfil();//this pas necessaire finalement il est sous entendu
	cout << "\t\t" << "jours restants:\t" << joursRestants_ << endl;
}

/********************************************
*La fonction modifierJoursRestants modifie le parametre le nombre de jour restant au statuts premuim du client
*
*param joursRestants : Nouveaux joursRestants_.
*******************************************/
void ClientPremium::modifierJoursRestants(unsigned int joursRestants)
{
    joursRestants_ = joursRestants_;
}

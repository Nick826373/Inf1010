/********************************************
* Titre: Travail pratique #4 - Client.cpp
* Date: 11 mars 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Client.h"
#include "ProduitAuxEncheres.h"
#include <iostream>

/********************************************
*Le premier constructeur par param�tres de la classe ClientPremium initialise les attributs h�riter d'usager � leur
*valeur par default et le codeClient passer � la valeur passer en param�tre
*******************************************/
Client::Client(unsigned int codeClient)
    : Usager(),
      codeClient_(codeClient)
{
}

/********************************************
*Le constructeur par param�tres de la Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Client::Client(const string &nom, const string &prenom, int identifiant,
               const string &codePostal, unsigned int codeClient)
    : Usager(nom, prenom, identifiant, codePostal),
      codeClient_(codeClient)
{
}

/********************************************
*La fonction obtenirCodeClient retourne la valeur du parametre codeClient_
*
*return : Retourne l'code du client.
*******************************************/
unsigned int Client::obtenirCodeClient() const
{
    return codeClient_;
}

/********************************************
*La fonction obtenirPanier retourne la valeur du parametre Panier_
*
*return : Retourne le vecteur de pointeur des produits du panier.
*******************************************/
vector<Produit *> Client::obtenirPanier() const
{
    return panier_;
}

/********************************************
*La fonction obtenirTotalApayer retourne le total du prix des items du client.
*
*return : Retourne la somme des prix des objets du panier.
*******************************************/
double Client::obtenirTotalAPayer() const
{
	double total = 0;

	for (unsigned i = 0; i < panier_.size(); i++){
		total += panier_[i]->obtenirPrix();
	}
	return total;
}

/********************************************
*La fonction afficherPanier permet d'afficher le contenu du panier d'un client facilement dans la console.
*******************************************/
void Client::afficherPanier() const
{
    cout << "PANIER (de " << obtenirNom() << ")"
         << "\n";
    for (unsigned int i = 0; i < panier_.size(); i++)
        panier_[i]->afficher();
    cout << endl;
}

/********************************************
*La fonction afficherProfil permet d'afficher le profil(attribut autre que le panier) d'un client facilement dans la console.
*******************************************/
void Client::afficherProfil() const
{
	Usager::afficherProfil();
	cout << "\t\t" << "code client:" << "\t" << codeClient_ << endl
	     << "\t\t" << "panier:\t\t" << panier_.size() << " " << "elements" << endl;
}

/********************************************
*La fonction modifierCodeClient modifie le parametre codeClient_
*
*param codeClient : Nouveaux codeClient_.
*******************************************/
void Client::modifierCodeClient(unsigned int codeClient)
{
    codeClient_ = codeClient;
}

/********************************************
*La fonction enleverProduit enleve un produit du panier du client.
*
*param produit : le pointeur vers le produit � enlever.
*******************************************/
void Client::enleverProduit(Produit *produit)
{
	//parcours le panier pour trouver l'indice du produit
    for (unsigned int i = 0; i < panier_.size(); i++)
    {
        if (panier_[i] == produit)
        {
			//supprime le produit du panier
            panier_[i] = panier_[panier_.size() - 1];
            panier_.pop_back();
            return;
        }
    }
}

/********************************************
*La fonction ajouterProduit ajoute un produit au panier du client.
*
*param produit : le pointeur vers le produit � ajouter.
*******************************************/
void Client::ajouterProduit(Produit *produit)
{
	//v�rifie si le produit est deja dans le panier
    for (unsigned int i = 0; i < panier_.size(); i++)
        if (panier_[i] == produit)
            return;
	//ajoute le produit au panier
    panier_.push_back(produit);
}

/********************************************
*La fonction r�initialiser permet de vider le panier d'un client
*******************************************/
void Client::reinitialiser()
{
	//si le produit est un produit au enchere on remet son prix et son encherisseur � leur valeur par default
	for (unsigned i = 0; i < panier_.size(); i++)
	{
		ProduitAuxEncheres *produit = dynamic_cast<ProduitAuxEncheres*>(panier_[i]);
		if (produit != NULL) {
			produit->mettreAJourEnchere(nullptr, produit->obtenirPrixInitial());
		}
	}
	//vide le panier
	panier_.clear();
}

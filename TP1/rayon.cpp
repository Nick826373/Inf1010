/********************************************
* Titre: Travail pratique #1 - Rayon.h
* Date: 20 janvier 2018
* Auteur :
*******************************************/

#include <string>
#include <iostream>
#include "Produit.h"
#include "Rayon.h"

using namespace std;

Rayon::Rayon() {

	categorie_ = "inconnu";
	tousProduits_ = nullptr;
	capaciteProduits_ = 0;
	nombreProduits_ = 0;
}

Rayon::Rayon(string categorie) {

	categorie_ = categorie;
	capaciteProduits_ = 0;
	nombreProduits_ = 0;
	tousProduits_ = nullptr;
}

Rayon::~Rayon() {
	for (int i = 0; i < capaciteProduits_; i++) {
		delete tousProduits_[i];
	}
	delete tousProduits_;
}

// Methodes d'acces

string Rayon::obtenirCategorie() const {
	return categorie_;
}
Produit ** Rayon::obtenirTousProduits() const {
	return tousProduits_;
}
int Rayon::obtenirCapaciteProduits() const {
	return capaciteProduits_;
}
int Rayon::obtenirNombreProduits() const {
	return nombreProduits_;
}


// Methodes de modification

void Rayon::modifierCategorie(string cat) {
	categorie_ = cat;
}

void Rayon::ajouterProduit(Produit * produit) {
	if (capaciteProduits_ = 0) {
		initialiserTableau();
	}
	else if (capaciteProduits_ <= nombreProduits_) {
		augmenterTableau();
	}

	tousProduits_[nombreProduits_++] = produit;
}

void Rayon::afficher() const {
	cout << "**********************************************"; //46
	cout << "* Le rayon:" << categorie_ << " contient: ";
	for (int i = 0; i < 21-categorie_.size(); i++)
	{
		cout << " ";
	}
	cout << "*"<<endl;

	for (int i = 0; i < nombreProduits_; i++){

		cout << "* ";
		int nbLettres = tousProduits_[i]->afficher();
		for (int i = 0; i < 42 - nbLettres; i++) {
			cout << " ";
		}
		cout << " *" << endl;
	}
	cout << "**********************************************";
}

void Rayon::initialiserTableau() {
	capaciteProduits_ = CAPACITE_INITIAL;
	tousProduits_ = new Produit*[capaciteProduits_];
}

void Rayon::augmenterTableau() {
	capaciteProduits_ += CAPACITE_AJOUTER;
	Produit ** TableauTemp = new Produit*[capaciteProduits_];
	for (int i = 0; i < nombreProduits_; i++) {

		TableauTemp[i] = tousProduits_[i];
	}
	for (int i = 0; i < nombreProduits_; i++) {
		delete tousProduits_[i];
	}
	tousProduits_ = new Produit*[capaciteProduits_];
	for (int i = 0; i < nombreProduits_; i++) {

		 tousProduits_[i]=TableauTemp[i];
	}
}
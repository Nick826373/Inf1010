//  panier.cpp
//  Created by Martine Bellaiche on 18-01-22.
//  Copyright � 2018 Martine Bellaiche. All rights reserved.
//

#include <iostream>
#include <string>
#include "Produit.h"
#include "Panier.h"

Panier::Panier(int capacite) {
	capaciteContenu_ = capacite;
	nombreContenu_ = 0;
	totalAPayer_ = 0;
	contenuPanier_ = new Produit*[capaciteContenu_];
}

Panier::~Panier() {
	delete[] contenuPanier_;
}

// methodes d'acc�s
Produit **  Panier::obtenirContenuPanier() const {

	return contenuPanier_;
}

int Panier::obtenirNombreContenu() const {

	return nombreContenu_;
}
double Panier::obtenirTotalApayer() const {

	return totalAPayer_;
}


// autres m�thodes
void Panier::ajouter(Produit * prod) {
	if (capaciteContenu_ < nombreContenu_)
		doublerCapacite();
	contenuPanier_[nombreContenu_++] = prod;
	totalAPayer_ += prod->obtenirPrix;
}


void Panier::livrer() {

	for (int i = 0; i < nombreContenu_;i++)
		delete contenuPanier_[i];

	nombreContenu_ = 0;
	totalAPayer_ = 0;
}


void Panier::afficher() const{
	cout << "Panier";
}

void Panier::doublerCapacite() {

	capaciteContenu_ *= 2;
	Produit ** TableauTemp = new Produit*[capaciteContenu_];
	for (int i = 0; i < nombreContenu_; i++) {

		TableauTemp[i] = contenuPanier_[i];
	}
	for (int i = 0; i < nombreContenu_; i++) {
		delete contenuPanier_[i];
	}
	contenuPanier_ = new Produit*[capaciteContenu_];
	for (int i = 0; i < nombreContenu_; i++) {

		contenuPanier_[i] = TableauTemp[i];
	}
}
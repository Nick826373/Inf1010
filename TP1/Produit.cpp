/********************************************
* Titre: Travail pratique #1 - Produit.h
* Date: 20 janvier 2018
* Auteur: Mohammed Esseddik BENYAHIA
*******************************************/

#include <string>
#include "Produit.h"
#include <iostream>
using namespace std;

Produit::Produit() {
	
	nom_ = "outils";
	prix_ = 0;
	reference_ = 0;
}

Produit::Produit(string nom, int reference, double prix) {

	nom_ = nom;
	reference_ = reference;
	prix_ = prix;
}

string Produit::obtenirNom() const {

	return nom_;
}
int Produit::obtenirReference() const {

	return reference_;

}

double Produit::obtenirPrix() const {


	return prix_;
}

void Produit::modifierNom(string nom) {

	nom_ = nom;
}
void Produit::modifierReference(int reference) {

	reference_ = reference;
}
void Produit::modifierPrix(double prix) {

	prix_ = prix;
}

int Produit::afficher() const {
	cout << "#" << reference_ << " " << nom_ << " " << prix_ << "$";

	int nbChiffre = 0;
	int indice = (int)prix_;
	while (indice != 0) {
		indice /= 10; nbChiffre++;
	}
	return 11 + nom_.size() + nbChiffre;
}
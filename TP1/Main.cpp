/**************************************************
* Titre: Travail pratique #1 - Main.cpp
* Date: 20 janvier 2018
* Auteur: Mohammed Esseddik BENYAHIA
**************************************************/

#include "Produit.h"
#include "Rayon.h"
#include "client.h"
#include "panier.h"
#include <string>
#include <iostream>

using namespace std;

int main()
{
	//Constante
	const int NB_OBJETS_DEPART = 15;

	string nomProduits[NB_OBJETS_DEPART] = { "Tondeuse", "laveuse", "four", "frigo", "marmitte", "poele", "fourchette", "cuillere", "couteau", "couvre-lit", "tableau", "table," "chaise", "coussin", "ordinateur" };

	//C'est à vous de voir si vous devez allouer dynamiquement ou non les élèments

	//1-  Creez 15 objets du classe produit
	Produit** listeDeProduits = new Produit*[NB_OBJETS_DEPART];
	for (int i = 0; i < NB_OBJETS_DEPART; i++) {
		listeDeProduits[i] = new Produit(nomProduits[i], 1500 + i, 10.00 + i*1.50);
	}
    
    
	//2-  Modifiez le nom, la référence, le prix de  troisieme objet Produit créé
    //   afficher les attributs de cet objet Produit
	
	listeDeProduits[2]->modifierNom("fourAConvection");
	listeDeProduits[2]->modifierReference(1600);
	listeDeProduits[2]->modifierPrix(100.00);
	
	//3-  Creez un objet du classe rayon à l'aide du constructeur par défaut

	Rayon rayonParDefault;
   
	//4-  Modifiez la catégorie  du rayon

	rayonParDefault.modifierCategorie("Cuisisne");
   
    // 5- Ajouter 6 produits de  votre choix dans le rayon créé
    
	for (int i = 0; i < 6; i++) {

		rayonParDefault.ajouterProduit(listeDeProduits[i]);
	}

    // 6- afficher le contenu du rayon
   
	rayonParDefault.afficher();
  
	//7-  Creez un objet de classe client à l'aide du constructeur
   
	Client client("Lavigueure", "Guetant", 123456, "h0h0h0", 199501);

	//8-  afficher l'etat des attributs du client
	client.afficherEtat();

	//9-   Le client achete 9 produits
	for (int i = 0; i < 9; i++)
	{
		client.acheter(listeDeProduits[i + 6]);
	}

	//10- Afficher le contenu du panier du client
	client.afficherPanier();

     //11- livrer le panier du client
	client.livrerPanier();
    
	//12- afficher le contenu du panier du client
	client.afficherPanier();
	//13-  terminer le programme correctement
    return 0;
}

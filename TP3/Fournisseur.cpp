/********************************************
* Titre: Travail pratique #3 - Fournisseur.cpp
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Fournisseur.h"

/********************************************
*Le constructeur par param�tres de la Fournisseur initialise les attributs aux valeurs passer en param�tre ou celle par default
*******************************************/
Fournisseur::Fournisseur(const string & nom, const string & prenom, int identifiant, const string & codePostal):
	Usager(nom, prenom, identifiant, codePostal), satisfaction_{ {0,0,0,0,0} }
{
}

// Methodes d'access
/********************************************
*La fonction obtenirCatalogue retourne la valeur du parametre contenuCatalogue_
*
*return : Retourne le vecteur contenant les produits du fournisseur.
*******************************************/
vector<Produit*> Fournisseur::obtenirCatalogue() const
{
	return contenuCatalogue_;
}

/********************************************
*La fonction obtenirSatisfaction retourne la valeur du parametre satisfaction_
*
*return : Retourne le tableau de note de satisfaction du fournisseur.
*******************************************/
Satisfaction Fournisseur::obtenirSatisfaction() const
{
	return satisfaction_;
}

// Methodes de modification
/********************************************
*La fonction modifierSatisfaction modifie le parametre satisfaction_
*
*param satisfaction : satisfaction_.
*******************************************/
void Fournisseur::modifierSatisfaction(Satisfaction satisfaction)
{
	satisfaction_ = satisfaction;
}

/********************************************
*La fonction noter incr�mente la case du tableau de satisfaction du fournisseur correspondant � la note
*
*param appreciation : la note recu.
*******************************************/
void Fournisseur::noter(int appreciation)
{
	satisfaction_.niveaux_[appreciation]++;
}

/********************************************
*La fonction ajouterProduit ajoute un produit dans le catatogue du fournisseur 
*
*param produit : le pointeur vers le produit � ajouter.
*******************************************/
void Fournisseur::ajouterProduit(Produit * produit)
{
	contenuCatalogue_.push_back(produit);
}

/********************************************
*La fonction enleverProduit enleve un produit dans le catatogue du fournisseur
*
*param produit : le pointeur vers le produit � enlever.
*******************************************/
void Fournisseur::enleverProduit(Produit * produit)
{
	for (unsigned i = 0; i < contenuCatalogue_.size(); i++)
	{
		if (contenuCatalogue_[i] == produit)
			contenuCatalogue_.erase(contenuCatalogue_.begin()+i);
	}
}

/********************************************
*La fonction operator= permet d'affecter la valeur d'un fournisseur a un autre.
*la fonction utilise l'operator= de la fonction parente
*
*param fournisseur: le fournisseur copier�<
*return: Le fournisseur apr�s la copie permettant de faire un appel en chaine
*******************************************/
Fournisseur & Fournisseur::operator=(const Fournisseur & fournisseur)
{
	static_cast<Usager>(*this) = static_cast<Usager>(fournisseur);
	satisfaction_=fournisseur.satisfaction_;
	contenuCatalogue_ = fournisseur.contenuCatalogue_;
	return *this;
}

/********************************************
*La fonction operator<< permet d'afficher un fournisseur facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param fournisseur: le client fournisseur
*
*return: le stream contenant les information du fournisseur a afficher
*******************************************/
ostream & operator<<(ostream & os, Fournisseur & fournisseur)
{
	os << "Fournisseur :"
		<< static_cast<Usager>(fournisseur)
		<< "\t    notes 0: " << fournisseur.obtenirSatisfaction().niveaux_[0] << endl
		<< "\t \t 1: " << fournisseur.obtenirSatisfaction().niveaux_[1] << endl
		<< "\t \t 2: " << fournisseur.obtenirSatisfaction().niveaux_[2] << endl
		<< "\t \t 3: " << fournisseur.obtenirSatisfaction().niveaux_[3] << endl
		<< "\t \t 4: " << fournisseur.obtenirSatisfaction().niveaux_[4] << endl;
	return os;
}

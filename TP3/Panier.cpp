/********************************************
* Titre: Travail pratique #3 - Panier.cpp
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/

#include "Panier.h"
#include <iomanip>
using namespace std;

/********************************************
*Le constructeur par paramètres de la Panier initialise le total a zero, l'idclient est celui passer par paramètre et le constructeur par default du vecteur contenuPanier est appelé
*******************************************/
Panier::Panier(int idClient) :
	totalAPayer_{ 0 }, idClient_{ idClient }, contenuPanier_{}
{
	
}

/********************************************
*Le destructeur de la classe Panier appele la méthode clear du vecteur contenuPanier_
*******************************************/
Panier::~Panier()
{
	contenuPanier_.clear();
}

// methodes d'accès
/********************************************
*La fonction obtenirContenuPanier retourne la valeur du parametre contenuPanier_
*
*return : Retourne le vecteur de pointeur vers des produits.
*******************************************/
vector<Produit*>  Panier::obtenirContenuPanier()const
{
	return contenuPanier_;
}

/********************************************
*La fonction obtenirNombreContenu retourne le nombre de produit contenu dans le panier
*
*return : Retourne le nombre de Produit pointer dans le vecteur contenuPanier_.
*******************************************/
int Panier::obtenirNombreContenu() const
{
	return (int)contenuPanier_.size();
}

/********************************************
*La fonction obtenirIdClient retourne la valeur du parametre idClient_
*
*return : Retourne l'identifiant du client qui possède ce panier.
*******************************************/
int Panier::obtenirIdClient() const {
	return idClient_;
}

/********************************************
*La fonction obtenirTotalApayer retourne la valeur du parametre totalAPayer_
*
*return : Retourne la somme des prix des objets du panier exluant les produit aux enchères.
*******************************************/
double Panier::obtenirTotalApayer() const {
	return totalAPayer_;
}

/********************************************
*La fonction calculerTotalApayer retourne la valeur du parametre totalAPayer_ additionner au prix des produits aux enchère qui sont encore emporté par le client possédant ce panier
*
*return : Retourne la somme des prix des objets du panier incluant les produit aux enchères remportés.
*******************************************/
double Panier::calculerTotalApayer()  const
{
	double totalAPayer = totalAPayer_;
	for (unsigned i = 0; i < contenuPanier_.size(); i++)
	{
		if (contenuPanier_[i]->retournerType() == 1) {
			if (static_cast<ProduitAuxEncheres*>(contenuPanier_[i])->obtenirIdentifiantClient() == idClient_) {
				 totalAPayer+= static_cast<ProduitAuxEncheres*>(contenuPanier_[i])->obtenirPrixBase();//vraiment constant??
			}
		}
	}
	return totalAPayer;
}

// méthodes de modification
/********************************************
*La fonction modifierTotalAPayer modifie le parametre totalAPayer_
*
*param totalAPayer : Nouveaux totalAPayer_.
*******************************************/
void Panier::modifierTotalAPayer(double totalAPayer)
{
	totalAPayer_ = totalAPayer;
}

// méthodes de modification
/********************************************
*La fonction modifierIdClient modifie le parametre idClient_
*
*param idClient : Nouveaux idClient_.
*******************************************/
void Panier::modifierIdClient(int idClient) {
	idClient_ = idClient;
}

/********************************************
*La fonction ajouter ajoute un pointeur vers un produit passer en paramètre au vecteur contenuPanier_.
*Le prix des produits ordinaires (incluant la taxe si applicable) est ajouté au totalAPayer_
*
*param prod : Le pointeur a ajouter.
*******************************************/
void Panier::ajouter(Produit * prod)
{
	if (prod->retournerType() == 0) {
		if (static_cast<ProduitOrdinaire*>(prod)->obtenirEstTaxable()) {
			totalAPayer_ += (prod->obtenirPrix()*(1+TAUX_TAXE));
		}		
	}
	contenuPanier_.push_back(prod);
}

/********************************************
*La fonction livrer vide le vecteur contenuPanier_ en ne supprimant pas les Produit pointer puisqu'ils appartiennent au main.
*******************************************/
void Panier::livrer()
{
	totalAPayer_ = 0;
	contenuPanier_.clear();
}

/********************************************
*La fonction retirerEncherePerdu retire du vecteur contenuPanier_ tous les produits aux enchères dont l'enchère est perdu
*
*return true si un produit à été retirer
*******************************************/
bool Panier::retirerEncherePerdu(){
	bool produitRetirer = false;
	if (contenuPanier_.empty())
		return produitRetirer;
	for (unsigned i = 0; i < contenuPanier_.size(); i++){
		if (contenuPanier_[i]->retournerType() == 1) {
			if (static_cast<ProduitAuxEncheres*>(contenuPanier_[i])->obtenirIdentifiantClient() != idClient_) {
				contenuPanier_[i] = contenuPanier_[contenuPanier_.size() - 1];
				contenuPanier_.pop_back();
				produitRetirer = true;
			}
		}
	}
	return produitRetirer;
}

/********************************************
*La fonction trouverProduitPlusCher retourne le produit le plus cher du panier.
*
*return: Un pointeur pointant vers le produit le plus cher du panier.
*******************************************/
Produit* Panier::trouverProduitPlusCher() 
{
	if (contenuPanier_.empty())
		return nullptr;
	retirerEncherePerdu();
	Produit* prodPlusChere = contenuPanier_[0];
	for (unsigned i = 1; i < contenuPanier_.size(); i++) {
		if (*contenuPanier_[i] > *prodPlusChere)
			prodPlusChere = contenuPanier_[i];
	}
	return prodPlusChere;
}

/********************************************
*La fonction operator<< permet d'afficher un panier facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param panier: le panier afficher
*
*return: le stream contenant les information du panier a afficher
*******************************************/
ostream & operator<<(ostream & os,  const Panier & panier)
{
	for (unsigned i = 0; i < panier.obtenirContenuPanier().size(); i++){
		os << *(static_cast<ProduitOrdinaire*>(panier.obtenirContenuPanier()[i])) << endl;
	}
	os << "--->total a payer : " << fixed << setprecision(3)<<panier.calculerTotalApayer();
	return os;
}

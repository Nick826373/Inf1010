/********************************************
* Titre: Travail pratique #3 - Produit.cpp
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656) 
*******************************************/

#include "Produit.h"
#include <iomanip>

/********************************************
*Le constructeur par param�tres de la Produit initialise les attributs aux valeurs passer en param�tre ou celle par default
*******************************************/
Produit::Produit(Fournisseur& fournisseur,const string& nom, int reference, double prix,TypeProduit type) :
	nom_{ nom }, fournisseur_{ fournisseur }, reference_{ reference }, prix_{ prix }, type_{type}
{
	fournisseur.ajouterProduit(this);
}

/********************************************
*Le destructeur de la classe Produit supprime le produit de la liste de produit de son fournisseur
*******************************************/
Produit::~Produit() {
	fournisseur_.enleverProduit(this);
}

// Methodes d'acces
/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom du produit.
*******************************************/
string Produit::obtenirNom() const
{
	return nom_;
}

/********************************************
*La fonction obtenirReference retourne la valeur du parametre reference_
*
*return : Retourne le numero reference du produit.
*******************************************/
int Produit::obtenirReference() const
{
	return reference_;
}

/********************************************
*La fonction obtenirPrix retourne la valeur du parametre prix_
*
*return : Retourne le prix du produit.
*******************************************/
double Produit::obtenirPrix() const
{
	return prix_;
}

/********************************************
*La fonction obtenirFournisseur retourne la valeur du parametre fournisseur_
*
*return : Retourne le fournisseur du produit.
*******************************************/
Fournisseur& Produit::obtenirFournisseur() const
{
	return fournisseur_;
}

/********************************************
*La fonction retournerType retourne la valeur du parametre type_
*
*return : Retourne le type du produit.
*******************************************/
TypeProduit Produit::retournerType() const{
	return type_;
}

// Methodes de modification
/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void Produit::modifierNom(const string& nom)
{
	nom_ = nom;
}

/********************************************
*La fonction modifierReference modifie le parametre reference_
*
*param reference : reference_.
*******************************************/
void Produit::modifierReference(int reference)
{
	reference_ = reference;
}

/********************************************
*La fonction modifierPrix modifie le parametre prix_
*
*param prix : prix_.
*******************************************/
void Produit::modifierPrix(double prix)
{
	prix_ = prix;
}

/********************************************
*La fonction operator> qui compare deux produits, l�un est plus grand que l�autre lorsque son prix est sup�rieur (strictement) au prix du deuxi�me.
*
*param produit.prix_ : prix_
*******************************************/
bool Produit::operator>(const Produit & produit) const
{
	return prix_ > produit.prix_;
}

/********************************************
*La fonction operator< qui compare deux produits, l�un est plus petit que l�autre lorsque son prix est inf�rieur (strictement) au prix du deuxi�me.
*
*param produit.prix_ : prix_
*******************************************/
bool Produit::operator<(const Produit & produit) const
{
	return prix_ < produit.prix_;
}

/********************************************
*La fonction operator== prend en param�tre un objet de la classe Produit
*et permet de comparer les attributs(nom, reference et prix) de 2 produits de la m�me classe.
*
*param produit.nom_: nom_ && param produit.reference_: reference_ && param produit.prix_: prix_
*******************************************/
bool Produit::operator==(const Produit & produit) const
{
	return (nom_ == produit.nom_ &&
			prix_ == produit.prix_ &&
			reference_ == produit.reference_);
}

// pour lire un type enumer�
inline istream & operator >> (istream & is, TypeProduit & typeProduit) {
	unsigned int type = 0;
	if (is >> type)
		typeProduit = static_cast<TypeProduit>(type);
	return is;
}

/********************************************
*La fonction operator>> permet de saisir en entr�e les attributs (nom, ref, prix) d�un objet de la classe Produit.
*
*param i : le stream lire
*
*param produit: le produit lu
*
*return: le stream contenant les information du produit � lire
*******************************************/
istream & operator>>(istream & is, Produit & produit)
{
	return is >> produit.nom_ >> produit.reference_ >> produit.prix_ >> produit.type_;
}

/********************************************
*La fonction operator<< permet d'afficher un produit facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param produit: le panier afficher
*
*return: le stream contenant les information du produit a afficher
*******************************************/
 ostream & operator<<(ostream & os, const Produit & produit)
{
	 os << "Produit :"
		 << " nom: " << produit.obtenirNom() << endl
		 << " \t ref : " << produit.obtenirReference() << endl
		 << " \t prix actuel : " << fixed << setprecision(0) <<produit.obtenirPrix() << endl
		 << "\t Fournisseur " << produit.obtenirFournisseur().obtenirNom() << endl;
	 return os;
}



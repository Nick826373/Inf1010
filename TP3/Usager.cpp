/********************************************
* Titre: Travail pratique #3 - Client.h
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/

#include <iostream>
#include "Usager.h"
using namespace std;

/********************************************
*Le constructeur par param�tres de la classe Usager initialise les attributs passer en parram�tre ou � leur valeur par default
*
*******************************************/
Usager::Usager(const string&  nom, const string& prenom, int identifiant, const string& codePostal):
	nom_{ nom },
	prenom_{ prenom },
	identifiant_{ identifiant },
	codePostal_{ codePostal }	
{
}

// Methodes d'access
/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom du client.
*******************************************/
string Usager::obtenirNom() const
{
	return nom_;
}

/********************************************
*La fonction obtenirPrenom retourne la valeur du parametre prenom_
*
*return : Retourne le prenom du client.
*******************************************/
string Usager::obtenirPrenom() const
{
	return prenom_;
}

/********************************************
*La fonction obtenirIdentifiant retourne la valeur du parametre Identifiant_
*
*return : Retourne l'identifiant de l'usager.
*******************************************/
int Usager::obtenirIdentifiant() const
{
	return identifiant_;
}

/********************************************
*La fonction obtenirCodePostal retourne la valeur du parametre codePostal_
*
*return : Retourne le code postale de l'usager.
*******************************************/
string Usager::obtenirCodePostal() const
{
	return codePostal_;
}

// Methodes de modification
/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void  Usager::modifierNom(const string& nom)
{
	nom_ = nom;
}

/********************************************
*La fonction modifierPrenom modifie le parametre prenom_
*
*param prenom : Nouveaux prenom_.
*******************************************/
void Usager::modifierPrenom(const string& prenom)
{
	prenom_ = prenom;
}

/********************************************
*La fonction modifierIdentifiant modifie le parametre identifiant_
*
*param identifiant : Nouveaux identifiant_.
*******************************************/
void Usager::modifierIdentifiant(int identifiant)
{
	identifiant_ = identifiant;
}

/********************************************
*La fonction modifierCodePostal modifie le parametre codePostal_
*
*param codePostal : Nouveaux codePostal_.
*******************************************/
void Usager::modifierCodePostal(const string& codePostal)
{
	codePostal_ = codePostal;
}

/********************************************
*La fonction operator= permet d'affecter la valeur d'un Usager a un autre.
*
*param client: l'usager � copier
*******************************************/
Usager &Usager::operator=(const Usager & usager)
{
	if (this != &usager) {
		nom_ = usager.nom_;
		prenom_ = usager.prenom_;
		identifiant_ = usager.identifiant_;
		codePostal_ = usager.codePostal_;		
	}
	return *this;
}

/********************************************
*La fonction operator== permet de comparer diractement l'identifiant d'un usager sans acc�der au param�tre identifiant_
*
*param identifiant: l'identifiant a comparer
*******************************************/
bool Usager::operator==(int identifiant) const {
	return identifiant_ == identifiant;
}

/********************************************
*La fonction operator== utilise la premiere surcharge pour comparer l'identifiant de deux usager sans acc�der au param�tre identifiant_
*
*
*param usager: l'usager a comparer
*******************************************/
bool Usager::operator == (const Usager & usager)
{
	return  *this == usager.obtenirIdentifiant();
}

/********************************************
*La fonction operator== est la m�me que la premi�re avec l'identifiant � gauche
*
*param identifiant: l'identifiant a comparer
*param usager: l'usager a comparer
*******************************************/
bool operator==(int indentifiant, const Usager& usager) {
	return usager == indentifiant;
}

/********************************************
*La fonction operator<< permet d'afficher un usager facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param usager: l'usager affich�
*
*return: le stream contenant les information du client a afficher
*******************************************/
ostream & operator<<(ostream & os, const Usager & usager)
{
	os << " Usager :";
	os << "\t nom : " << usager.obtenirNom() << endl
	   << "\t prenom : " << usager.obtenirPrenom() << endl
	   << "\t codePostal: " << usager.obtenirCodePostal() << endl;
	return os;
}


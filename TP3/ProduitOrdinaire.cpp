/********************************************
* Titre: Travail pratique #3 - ProduitOrdinaire.h
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/

#include <string>
#include <iostream>
#include "ProduitOrdinaire.h"
#include "Fournisseur.h"
using namespace std;

/********************************************
*Le constructeur par param�tres de la ProduitOrdinaire initialise les attributs aux valeurs passer en param�tre ou celle par default
*******************************************/
ProduitOrdinaire::ProduitOrdinaire(Fournisseur & fournisseur, const string & nom, int reference, double prix, TypeProduit type, bool estTaxable):
	Produit{fournisseur, nom, reference, prix, type}, estTaxable_{estTaxable}
{
}

// Methodes d'acces
/********************************************
*La fonction obtenirEstTaxable retourne la valeur du parametre estTaxable_
*
*return : Retourne true si le produit est taxable.
*******************************************/
bool ProduitOrdinaire::obtenirEstTaxable() const
{
	return estTaxable_;
}

// Methodes de modification
/********************************************
*La fonction modifierEstTaxable modifie le parametre estTaxable_
*
*param estTaxable : estTaxable_.
*******************************************/
void ProduitOrdinaire::modifierEstTaxable(bool estTaxable)
{
	estTaxable_ = estTaxable;
}

/********************************************
*La fonction operator<< permet d'afficher un ProduitOrdinaire facilement dans la console avec un cout.
*La fonction utilise l'operator<< de la classe parente Produit
*
*param o: le stream retourner
*
*param produit: le ProduitOrdinaire afficher
*
*return: le stream contenant les information du produit a afficher
*******************************************/
ostream & operator<<(ostream & os, const ProduitOrdinaire & produit)
{
	os << "ProduitOrdinaire "
		<< static_cast<Produit>(produit)
	    << "\t est Taxable: "<<boolalpha<<produit.obtenirEstTaxable()<<endl;
	return os;
}

/********************************************
*La fonction operator>> permet de saisir en entr�e les attributs d�un objet de la classe ProduitOrdinaire.
*La fonction utilise l'operator<< de la classe parente Produit
*
*param i : le stream lire
*
*param produit: le ProduitOrdinaire lu
*
*return: le stream contenant les information du ProduitOrdinaire � lire
*******************************************/
istream & operator>>(istream & is, ProduitOrdinaire & produit)
{
	is >> produit;//to fix
	is >> produit.estTaxable_;
	return is;
}

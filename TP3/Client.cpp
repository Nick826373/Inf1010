/**************************************************
* Titre: Travail pratique #3 - Client.cpp
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
**************************************************/

#include "Client.h"
#include "Fournisseur.h"
#include <stdlib.h>
#include<ctime>


/********************************************
*La class Client contient créer un client qui va acheter des produits et les ajouter dans
*son panier.
*
*Le constructeur par paramètres de la classe Cliente initialise les attributs et le pointeur de l’objet Panier à null.
*
*******************************************/
Client::Client(const string & nom, const string & prenom, int identifiant, const string & codePostal, long date):
	Usager(nom, prenom, identifiant, codePostal), dateNaissance_{date}, monPanier_{nullptr}
{
	srand((unsigned)time(NULL));
}

/********************************************
*Le destructeur de la classe Cliente supprime le pointeur de l’objet Panier
*******************************************/
Client::~Client()
{
	if (monPanier_ != nullptr)
		delete monPanier_;
}

/********************************************
*Le constructeur par copie de la classe Cliente prend en paramètre les valeurs initier précedement et les places dans un autre objet client
*La copy du panier est une deep copy
*******************************************/
Client::Client(const Client & client) :
	Usager(client.obtenirNom(), client.obtenirPrenom(), client.obtenirIdentifiant(), client.obtenirCodePostal()),	
	dateNaissance_{ client.dateNaissance_ }
{
	if (client.monPanier_ == nullptr)
		monPanier_ = nullptr;
	else {
		monPanier_ = new Panier(client.obtenirIdentifiant());
		for (int i = 0; i < client.monPanier_->obtenirNombreContenu(); i++) {
			monPanier_->ajouter(client.monPanier_->obtenirContenuPanier()[i]);
		}
		int idClient = this->obtenirIdentifiant();
		monPanier_->modifierTotalAPayer(client.monPanier_->obtenirTotalApayer());
	}
}


// Methodes d'acces
/********************************************
*La fonction obtenirDateNaissance retourne la valeur du parametre dateNaissance_
*
*return : Retourne la date de naissance du client
*******************************************/
long Client::obtenirDateNaissance() const
{
	return dateNaissance_;
}
/********************************************
*La fonction obtenirPanier retourne la valeur du parametre monPanier_
*
*return : Retourne le pointeur qui pointe vers les produits du panier.
*******************************************/
Panier * Client::obtenirPanier() const
{
	return monPanier_;
}


// Methodes de modification

/********************************************
*La fonction modifierDateNaissance modifie le parametre dateNaissance_
*
*param date : Nouveaux dateNaissance_.
*******************************************/
void Client::modifierDateNaissance(long date)
{
	dateNaissance_ = date;
}

// Autres méthodes
/********************************************
*La fonction acheter ajoute un pointeur vers un Produit dans son panier
*La méthode créé un nouveau panier de manière dynamique s'il n'existe pas
*Un note entre 0 et 4 est passer au fournisseur du produit
*
*param prod : Le pointeur a ajouter.
*******************************************/
void Client::acheter(ProduitOrdinaire * prod)
{
	if (monPanier_ == nullptr)
		monPanier_ = new Panier(this->obtenirIdentifiant());
	monPanier_->ajouter(prod);
	// obtenir une note aléatoire
	int note = (rand() %4) +1;
	// faire la mise à jour de la satisfaction au fournisseur
	prod->obtenirFournisseur().noter(note);
}

/********************************************
*La fonction livrerPanier supprime le panier du client
*******************************************/
void Client::livrerPanier()
{
	monPanier_->livrer();
	delete monPanier_;
	monPanier_ = nullptr;
}

/********************************************
*La fonction miser produit modifie le prix et l'IDclient d'un produit au enchere si le prix est supérieur 
*à la mise précédente
*
*param produitAuxEncheres : Le pointeur vers le produit aux enchères
*param montantMise : le montant miser par le client
*******************************************/
void Client::miserProduit(ProduitAuxEncheres* produitAuxEncheres, double montantMise) {
	if (montantMise > produitAuxEncheres->obtenirPrixBase()) {
		produitAuxEncheres->modifierPrixBase(montantMise);
		produitAuxEncheres->modifierIdentifiantClient(this->obtenirIdentifiant());
	}
	if (monPanier_ == nullptr)
		monPanier_ = new Panier(this->obtenirIdentifiant());
	monPanier_->ajouter(produitAuxEncheres);
}

/********************************************
*La fonction operator= permet d'affecter la valeur d'un client a un autre.
*La copy du panier est une deep copy
*
*param client: le client copier
*******************************************/
Client & Client::operator=(const Client & client)
{
	if (this != &client) {
		this->modifierNom(client.obtenirNom());
		this->modifierPrenom(client.obtenirPrenom());
		this->modifierIdentifiant(client.obtenirIdentifiant());
		this->modifierCodePostal(client.obtenirCodePostal());
		dateNaissance_ = client.obtenirDateNaissance();
		if (client.monPanier_ != nullptr) {
			delete monPanier_;
			monPanier_ = new Panier(obtenirIdentifiant());
			for (int i = 0; i < client.monPanier_->obtenirNombreContenu(); i++) {
				monPanier_->ajouter(client.monPanier_->obtenirContenuPanier()[i]);
			}
		}
		else
			monPanier_ = nullptr;
	}
	return *this;
}

/********************************************
*La fonction operator<< permet d'afficher un client facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param client: le client affiché
*
*return: le stream contenant les information du client a afficher
*******************************************/
ostream & operator<<(ostream & os, const Client & client)
{
	os << "Client: "
		<< static_cast<Usager>(client);
	if (client.obtenirPanier() == nullptr) {
		os << "Le panier de " << client.obtenirPrenom() << " est vide !" << endl;
	}
	else {
		client.monPanier_->retirerEncherePerdu();
		os << "Le panier de " << client.obtenirPrenom() << " :"<< endl << *(client.obtenirPanier()) << endl;
	}
	return os;
}

/********************************************
* Titre: Travail pratique #3 - ProduitAuxEncheres.cpp
* Date: 25 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/

#include "ProduitAuxEncheres.h"
#include<iomanip>

/********************************************
*Le constructeur par param�tres de la ProduitAuxEncheres initialise les attributs aux valeurs passer en param�tre ou celle par default
*******************************************/
ProduitAuxEncheres::ProduitAuxEncheres(Fournisseur & fournisseur, const string & nom, int reference, double prix, TypeProduit type) :
	Produit{ fournisseur, nom, reference, prix, type }, prixBase_{ prix }, identifiantClient_{ 0 }
{
}

// Methodes d'acces
/********************************************
*La fonction obtenirIdentifiantClient retourne la valeur du parametre identifiantClient_
*
*return : Retourne l'identifiant du client ayant la mise gagnante su le produit.
*******************************************/
int ProduitAuxEncheres::obtenirIdentifiantClient() const
{
	return identifiantClient_;
}

/********************************************
*La fonction obtenirPrixBase retourne la valeur du parametre prixBase_
*
*return : Retourne la mise la plus �lev� sur le produit.
*******************************************/
double ProduitAuxEncheres::obtenirPrixBase() const
{
	return prixBase_;
}

// Methodes de modification
/********************************************
*La fonction modifierIdentifiantClient modifie le parametre identifiantClient_
*
*param identifiantClient : identifiantClient_.
*******************************************/
void ProduitAuxEncheres::modifierIdentifiantClient(int identifiantClient)
{
	identifiantClient_ = identifiantClient;
}

/********************************************
*La fonction modifierPrixBase modifie le parametre prixBase_
*
*param prixBase : prixBase_.
*******************************************/
void ProduitAuxEncheres::modifierPrixBase(double prixBase)
{
	prixBase_ = prixBase;
}

/********************************************
*La fonction operator>> permet de saisir en entr�e les attributs d�un objet de la classe ProduitAuxEncheres.
*La fonction utilise l'operator<< de la classe parente Produit
*
*param i : le stream lire
*
*param produit: le ProduitAuxEncheres lu
*
*return: le stream contenant les information du ProduitAuxEncheres � lire
*******************************************/
istream & operator>>(istream & is, ProduitAuxEncheres & produit)
{
	is >> produit;//to fix
	return is;
}

/********************************************
*La fonction operator<< permet d'afficher un ProduitAuxEncheres facilement dans la console avec un cout.
*La fonction utilise l'operator<< de la classe parente Produit
*
*param o: le stream retourner
*
*param produit: le ProduitAuxEncheres afficher
*
*return: le stream contenant les information du produit a afficher
*******************************************/
ostream & operator<<(ostream & os, const ProduitAuxEncheres & produit)
{
	os << "ProduitEnchere "
		<< static_cast<Produit>(produit)
		<< "\t prixBase: " << fixed << setprecision(0) << produit.obtenirPrixBase() << endl
		<< "\t indentifiant Client: " << produit.obtenirIdentifiantClient() << endl;
	return os;
}

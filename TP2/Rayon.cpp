/**************************************************
* Titre: Travail pratique #2 - Rayon.cpp
* Date: 10 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
Le programme va crée un rayon où qui contient les produits du magasin.
**************************************************/
#include "Rayon.h"

/********************************************
*La class Rayon contient un vecteur de pointeur qui contiendra les différents produits. Elle contient aussi un paramètre qui garde
* en memoire le nombre de Produits.
*
*Le constructeur 
*******************************************/

Rayon::Rayon(const string& cat) :
	categorie_{ cat },
	tousProduits_{  },
	nombreProduits_{ 0 }
{
}

// Methodes d'acces

/********************************************
*La fonction obtenirCategorie retourne la valeur du parametre categorie_
*
*return : Retourne le nom de la catégorie
*******************************************/
string Rayon::obtenirCategorie() const
{
	return categorie_;
}
/********************************************
*La fonction obtenirobtenirTousProduits qui retourne la valeur du parametre tousProduits_
*
*return : Retourne le vecteur de pointeur vers des produits.
*******************************************/

vector<Produit*> Rayon::obtenirTousProduits() const
{
	return tousProduits_;
}
/********************************************
*La fonction obtenirNombreProduits retourne la valeur du parametre nombreProduits_
*
*return : Retourne le nombre de Produit pointer dans le vecteur tousProduits_.
*******************************************/

int Rayon::obtenirNombreProduits() const
{
	return nombreProduits_;
}

// Methodes de modification

/********************************************
*La fonction modifierCategorie modifie le parametre categorie_
*
*param cat : Nouveaux categorie_.
*******************************************/
void Rayon::modifierCategorie(const string& cat)
{
	categorie_ = cat;
}
/********************************************
*La fonction compterDoublons qui retourne le nombre de fois qu’un produit identique au produit passé en paramètre apparaisse dans le rayon.
*
*return: le produit qui apparaît en double.
*******************************************/
int Rayon::compterDoublons(const Produit & produit) const
{
	int compteurDoublons=0;
	for (int i = 0; i < tousProduits_.size(); i++)
	{
		if (*tousProduits_[i] == produit)
			compteurDoublons++;
	}
	return compteurDoublons;
}
/********************************************
*L'opérateur prend en paramètre un produit, qui l’ajoute au vecteur tousProduits_ et qui retourne le rayon après l’ajout de ce produit.
*
*param produit += vector<Produit*>
*
*return:  le rayon après l’ajout d'un produit dans le vecteur tousProduits_.
*******************************************/
Rayon Rayon::operator+=(Produit* produit)
{
	tousProduits_.push_back(produit);
	nombreProduits_ += 1;
	return *this;
}
/********************************************
*La fonction operator<< permet d'afficher un rayon facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param rayon: le rayon afficher
*
*return: le stream contenant les information du rayon a afficher
*******************************************/
ostream & operator<<(ostream & o, const Rayon & rayon)
{
	o << "Le rayon " << rayon.categorie_ << ": " << endl;
	for (int j = 0; j < rayon.nombreProduits_; j++) {
		o << "----> " << *(rayon.tousProduits_[j]);
	}
	return o;
}

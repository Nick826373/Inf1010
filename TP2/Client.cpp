/**************************************************
* Titre: Travail pratique #2 - Client.cpp
* Date: 10 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
**************************************************/

#include "Client.h"

/********************************************
*La class Client contient créer un client qui va acheter des produits et les ajouter dans
*son panier.
*
*Le constructeur par paramètres de la classe Cliente initialise les attributs et le pointeur de l’objet Panier à null.
*
*******************************************/
Client::Client(const string&  nom, const string& prenom, int identifiant, const string& codePostal, long date) :
	nom_{ nom },
	prenom_{ prenom },
	identifiant_{ identifiant },
	codePostal_{ codePostal },
	dateNaissance_{ date },
	monPanier_{new Panier()}
{
}
/********************************************
*Le constructeur par copie de la classe Cliente prend en paramètre les valeurs initier précedement et les places dans un autre objet client
*
*******************************************/
Client::Client(const Client& client) :
nom_ { client.nom_ },
prenom_{ client.prenom_ },
identifiant_{ client.identifiant_ },
codePostal_{ client.codePostal_ },
dateNaissance_{ client.dateNaissance_ },
monPanier_{ new Panier() }
{
	*monPanier_ = *client.monPanier_;		
}
/********************************************
*Le destructeur de la classe Cliente supprime le pointeur de l’objet Panier 
*******************************************/
Client::~Client() {
	delete monPanier_;
}

// Methodes d'access
/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom du client.
*******************************************/
string Client::obtenirNom() const
{
	return nom_;
}
/********************************************
*La fonction obtenirPrenom retourne la valeur du parametre prenom_
*
*return : Retourne le prenom du client.
*******************************************/
string Client::obtenirPrenom() const
{
	return prenom_;
}
/********************************************
*La fonction obtenirIdentifiant retourne la valeur du parametre Identifiant_
*
*return : Retourne l'identifiant du client.
*******************************************/
int Client::obtenirIdentifiant() const
{
	return identifiant_;
}
/********************************************
*La fonction obtenirCodePostal retourne la valeur du parametre codePostal_
*
*return : Retourne le code postale du client.
*******************************************/
string Client::obtenirCodePostal() const
{
	return codePostal_;
}
/********************************************
*La fonction obtenirDateNaissance retourne la valeur du parametre dateNaissance_
*
*return : Retourne la date de naissance du client
*******************************************/
long Client::obtenirDateNaissance() const
{
	return dateNaissance_;
}
/********************************************
*La fonction obtenirPanier retourne la valeur du parametre monPanier_
*
*return : Retourne le pointeur qui pointe vers les produits du panier.
*******************************************/
Panier * Client::obtenirPanier() const
{
	return monPanier_;
}

// Methodes de modification
/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void  Client::modifierNom(const string& nom)
{
	nom_ = nom;
}
/********************************************
*La fonction modifierPrenom modifie le parametre prenom_
*
*param prenom : Nouveaux prenom_.
*******************************************/
void Client::modifierPrenom(const string& prenom)
{
	prenom_ = prenom;
}
/********************************************
*La fonction modifierIdentifiant modifie le parametre identifiant_
*
*param identifiant : Nouveaux identifiant_.
*******************************************/
void Client::modifierIdentifiant(int identifiant)
{
	identifiant_ = identifiant;
}
/********************************************
*La fonction modifierCodePostal modifie le parametre codePostal_
*
*param codePostal : Nouveaux codePostal_.
*******************************************/
void Client::modifierCodePostal(const string& codePostal)
{
	codePostal_ = codePostal;
}
/********************************************
*La fonction modifierDateNaissance modifie le parametre dateNaissance_
*
*param date : Nouveaux dateNaissance_.
*******************************************/
void Client::modifierDateNaissance(long date)
{
	dateNaissance_ = date;
}

// Autres méthodes
/********************************************
*La fonction acheter ajoute pointe vers un objet de Produit de la focntion ajouter
*
*param prod : Le pointeur a ajouter.
*******************************************/
void Client::acheter(Produit * prod)
{
	monPanier_->ajouter(prod);
}

void Client::livrerPanier()
{
	monPanier_->livrer();
}
/********************************************
*La fonction operator= permet d'affecter la valeur d'un client a un autre.
*
*param client: le client copier
*******************************************/
void Client::operator=(const Client client)
{
	nom_ = client.nom_;
	prenom_ = client.prenom_;
	identifiant_ = client.identifiant_;
	codePostal_ = client.codePostal_;
	dateNaissance_ = client.dateNaissance_;
	delete monPanier_;
	monPanier_ = new Panier();
	*monPanier_ = *client.monPanier_;		
}
/********************************************
*La fonction operator== qui prend un entier en paramètre et permet de comparer un client avec son identifiant.
*
*param identifiant: l'identifiant_ 
*******************************************/
bool Client::operator==(const int identifiant) const
{
	return identifiant_ == identifiant;
}
/********************************************
*La fonction qui permet d’inverser l’ordre de l’opérateur== précédent
*
*param indentifiant: le client.identifiant
*******************************************/
bool operator==(const int identifiant, const Client & client)
{
	return client.identifiant_ == identifiant;
}
/********************************************
*La fonction operator<< permet d'afficher un client facilement dans la console avec un cout.
*
*param o: le stream retourner
*
*param client: le client affiché
*
*return: le stream contenant les information du client a afficher
*******************************************/
ostream& operator<<(ostream& o, const Client& client)
{
	if (client.monPanier_->obtenirNombreContenu() != 0) {
		cout << "Le panier de " << client.prenom_ << ": " << endl;
		cout << *(client.monPanier_);
	}
	else 
	{
		cout << "Le panier de " << client.prenom_ << " est vide !" << endl << endl;
	}		
	return o;
}

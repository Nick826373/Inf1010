/**************************************************
* Titre: Travail pratique #2 - Produit.cpp
* Date: 10 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
Le programme va cr�e des produit en lui donnant un nom, un prix et une ref�rence.
**************************************************/

#include "Produit.h"
/********************************************
*La class Produit cr�e un produit qui contien les caract�ristiques nom, prix et reference
*
*Le constructeur par param�tres de la classe Produits initialise les attributs
*
*******************************************/
Produit::Produit(const string& nom, int reference, double prix) :
	nom_{ nom },
	reference_{ reference },
	prix_{ prix }
{ }

// Methodes d'acces
/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom du produit.
*******************************************/
string Produit::obtenirNom() const
{
	return nom_;
}
/********************************************
*La fonction obtenirReference retourne la valeur du parametre reference_
*
*return : Retourne le numero reference du produit.
*******************************************/
int Produit::obtenirReference() const
{
	return reference_;
}
/********************************************
*La fonction obtenirPrix retourne la valeur du parametre prix_
*
*return : Retourne le prix du produit.
*******************************************/
double Produit::obtenirPrix() const
{
	return prix_;
}

// Methodes de modification
/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void Produit::modifierNom(const string& nom)
{
	nom_ = nom;
}
/********************************************
*La fonction modifierReference modifie le parametre reference_
*
*param reference : reference_.
*******************************************/
void Produit::modifierReference(int reference)
{
	reference_ = reference;
}
/********************************************
*La fonction modifierPrix modifie le parametre prix_
*
*param prix : prix_.
*******************************************/
void Produit::modifierPrix(double prix)
{
	prix_ = prix;
}
/********************************************
*La fonction operator> qui compare deux produits, l�un est plus petit que l�autre lorsque son prix est inf�rieur (strictement) au prix du deuxi�me.
*
*param produit.prix_ : prix_
*******************************************/
bool Produit::operator>(const Produit produit) const
{
	return prix_>produit.prix_;
}
/********************************************
*La fonction operator> qui compare deux produits, l�un est plus grand que l�autre lorsque son prix est sup�rieur (strictement) au prix du deuxi�me.
*
*param produit.prix_ : prix_
*******************************************/
bool Produit::operator<(const Produit produit) const
{
	return prix_<produit.prix_;
}
/********************************************
*La fonction operator== prend en param�tre un objet de la classe Produit
*et permet de comparer les attributs(nom, reference et prix) de 2 produits de la m�me classe.
*
*param produit.nom_: nom_ && param produit.reference_: reference_ && param produit.prix_: prix_
*******************************************/
bool Produit::operator==(const Produit produit) const
{
	return nom_==produit.nom_ && reference_ == produit.reference_ && prix_ == produit.prix_;
}
/********************************************
*La fonction operator>> permet de saisir en entr�e les attributs (nom, ref, prix) d�un objet de la classe Produit.
*
*param i : le stream lire
*
*param produit: le produit lu
*
*return: le stream contenant les information du produit � lire
*******************************************/
istream & operator>>(istream & i,Produit & produit)
{
	i >> produit.nom_;
	i >> produit.reference_;
	i >> produit.prix_;
	return i;
}
/********************************************
*La fonction operator<< permet d'afficher un produit facilement dans la console avec un cout.
*
*param o: le stream retourner 
*
*param produit: le panier afficher
*
*return: le stream contenant les information du produit a afficher
*******************************************/
ostream& operator<<(ostream& o, const Produit& produit)
{
	return o << "nom : " << produit.nom_
		<< "\t ref : " << produit.reference_
		<< "\t prix : " << produit.prix_ << endl;
}

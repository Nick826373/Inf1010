/********************************************
* Titre: Travail pratique #2 - Panier.cpp
* Date: 10 fevrier 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/

#include "Panier.h"

/********************************************
*La class Panier contient un vecteur de pointeur vers des Produits. Elle contient aussi un paramètre qui garde
* en memoire le nombre de Produits, ainsi que le prix total du panier.
*
*Le constructeur par default de la class Panier utilise le constructeur par default pour son vecteur.
*
*******************************************/
Panier::Panier():
	nombreContenu_(0),
	contenuPanier_(),
	totalAPayer_(0)
{
}

// methodes d'accès
/********************************************
*La fonction obtenirContenuPanier retourne la valeur du parametre contenuPanier_
*
*return : Retourne le vecteur de pointeur vers des produits.
*******************************************/
vector<Produit*>  Panier::obtenirContenuPanier()const
{
	return contenuPanier_;
}

/********************************************
*La fonction obtenirNombreContenu retourne la valeur du parametre nombreContenu_
*
*return : Retourne le nombre de Produit pointer dans le vecteur contenuPanier_.
*******************************************/
int Panier::obtenirNombreContenu() const
{
	return nombreContenu_;
}

/********************************************
*La fonction obtenirTotalApayer retourne la valeur du parametre totalAPayer_
*
*return : Retourne la somme des prix des objets du panier.
*******************************************/
double Panier::obtenirTotalApayer() const
{
	return totalAPayer_;
}

// méthodes de modification
/********************************************
*La fonction modifierTotalAPayer modifie le parametre totalAPayer_
*
*param totalAPayer : Nouveaux totalAPayer_.
*******************************************/
void Panier::modifierTotalAPayer(double totalAPayer)
{
	totalAPayer_ = totalAPayer;
}

// autres méthodes

/********************************************
*La fonction ajouter ajoute un pointeur passer en paramètre au vecteur.
*
*param prod : Le pointeur a ajouter.
*******************************************/
void Panier::ajouter(Produit * prod)
{
	nombreContenu_++;
	contenuPanier_.push_back(prod);
	totalAPayer_ += prod->obtenirPrix();
}

/********************************************
*La fonction livrer vide le vecteur contenuPanier_ en ne supprimant pas les Produit pointer puisqu'ils appartiennent au main.
*
*******************************************/
void Panier::livrer()
{
	nombreContenu_ = 0;
	totalAPayer_ = 0;
	contenuPanier_ .clear();
}

/********************************************
*La fonction trouverProduitPlusCher retourne le produit le plus cher du panier.
*
*return: Un pointeur pointant vers le produit le plus cher du panier.
*******************************************/
Produit * Panier::trouverProduitPlusCher() const
{
	double produitPlusCher = contenuPanier_[0]->obtenirPrix();
	int indicePanierPlusCher = 0;
	for (int i = 0; i < nombreContenu_; i++)
	{
		if (produitPlusCher < contenuPanier_[i]->obtenirPrix()) {
			produitPlusCher = contenuPanier_[i]->obtenirPrix();
			indicePanierPlusCher = i;
		}
	}
	return contenuPanier_[indicePanierPlusCher];
}

/********************************************
*La fonction operator= permet d'affecter la valeur d'un panier a un autre.
*
*param panier: le panier copier
*******************************************/
void Panier::operator=(const Panier panier)//enlever ca ou le constr copie
{
	nombreContenu_ = panier.nombreContenu_;
	totalAPayer_ = panier.totalAPayer_;
	contenuPanier_ = panier.contenuPanier_;
}

/********************************************
*La fonction operator<< permet d'afficher un panier facilement dans la console avec un cout.
*
*param o: le stream retourner 
*
*param panier: le panier afficher
*
*return: le stream contenant les information du panier a afficher
*******************************************/
ostream & operator<<(ostream & o, const Panier & panier)
{
	for (int i = 0; i < panier.nombreContenu_; i++)
		cout << *panier.contenuPanier_[i];
	cout << "----> total a payer : " << panier.totalAPayer_ << endl << endl;
	return o;
}

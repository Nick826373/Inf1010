/********************************************
* Titre: Travail pratique #6 - InvalidArgumentException.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/


/********************************************
*Le constructeur par default de la classe InvalidArgumentException utilise le constructeur par default de la classe logic_error
*******************************************/
InvalidArgumentException::InvalidArgumentException(string what) : logic_error(what)
{}

/********************************************
* Titre: Travail pratique #6 - Usager.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Usager.h"
#include <iostream>

/********************************************
*Le constructeur par param�tres de la classe Usager initialise les attributs de l'usager au valeur passer en param�tre
*******************************************/
Usager::Usager(const string &nom, const string &prenom, int identifiant,
               const string &codePostal)
    : nom_(nom),
      prenom_(prenom),
      identifiant_(identifiant),
      codePostal_(codePostal)
{
}

/********************************************
*Le destructeur de la classe Usager 
*******************************************/
Usager::~Usager(){}

/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom de l'usager.
*******************************************/
string Usager::obtenirNom() const
{
	return nom_;
}

/********************************************
*La fonction obtenirPrenom retourne la valeur du parametre prenom_
*
*return : Retourne le prenom de l'usager.
*******************************************/
string Usager::obtenirPrenom() const
{
	return prenom_;
}

/********************************************
*La fonction obtenirIdentifiant retourne la valeur du parametre identifiant_
*
*return : Retourne l'identifiant de l'usager.
*******************************************/
int Usager::obtenirIdentifiant() const
{
    return identifiant_;
}

/********************************************
*La fonction obtenirCodePostal retourne la valeur du parametre codePostal_
*
*return : Retourne le code postale de l'usager.
*******************************************/
string Usager::obtenirCodePostal() const
{
	return codePostal_;
}

/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void Usager::modifierNom(const string &nom)
{
	nom_ = nom;
}

/********************************************
*La fonction modifierPrenom modifie le parametre prenom_
*
*param prenom : Nouveaux prenom_.
*******************************************/
void Usager::modifierPrenom(const string &prenom)
{
	prenom_ = prenom;
}

/********************************************
*La fonction modifierIdentifiant modifie le parametre identifiant_
*
*param reference : Nouveaux identifiant_.
*******************************************/
void Usager::modifierIdentifiant(int identifiant)
{
    identifiant_ = identifiant;
}

/********************************************
*La fonction modifierCodePostal modifie le parametre codePostal_
*
*param codePostal : Nouveaux codePostal_.
*******************************************/
void Usager::modifierCodePostal(const string &codePostal)
{
    codePostal_ = codePostal;
}

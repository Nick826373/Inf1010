/********************************************
* Titre: Travail pratique #6 - Fournisseur.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Fournisseur.h"
#include <iostream>

/********************************************
*Le constructeur par default de la classe Fournisseur utilise le constructeur par default de la classe Usager
*******************************************/
Fournisseur::Fournisseur()
    : Usager()
{
}

/********************************************
*Le constructeur par param�tres de la classe Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Fournisseur::Fournisseur(const string &nom, const string &prenom, int identifiant,
                         const string &codePostal)
    : Usager(nom, prenom, identifiant, codePostal)
{
}

/********************************************
* Titre: Travail pratique #6 - Gestionnaire.cpp
* Date: 19 mars 2018
* Auteur: Nicolas Bouchard et Robin Kuchue
*******************************************/
#include <algorithm>
#include "Gestionnaire.h"

/********************************************
*La fonction obtenirUsagers permet de retourner l'atrtribut usager_ du gestionnaire
*
*return : l'atrtribut usager_ du gestionnaire
*******************************************/
vector<Usager *> Gestionnaire::obtenirUsagers() const
{
    return usagers_;
}

/********************************************
*La fonction obtenirUsagers permet de retourner le nombre d'usager contenu par le gestionnaire
*
*return : l'atrtribut usager_ du gestionnaire
*******************************************/
int Gestionnaire::obtenirNombreUsager() const
{
    return usagers_.size();
}

/********************************************
*La fonction obtenirUsagers permet de retourner un usager par son index
*
*param int index: index de l'usager a retourner
*return : l'usager a l'index passer en parametre
*******************************************/
Usager* Gestionnaire::obtenirUsager(int index)
{
    return usagers_.at(index);
}

/********************************************
*La fonction ajouterUsager ajoute un usager dans la liste du gestionnaire
*
*param Usager* usager : le pointeur vers l'usager a ajouter
*******************************************/
void Gestionnaire::ajouterUsager(Usager* usager)
{
    for (unsigned int i = 0; i < usagers_.size(); i++){
        if (usagers_[i] == usager){
            return;
        }
    }
    usagers_.push_back(usager);
    emit usagerAjoute(usager);
}

/********************************************
*La fonction supprimerUsager enleve un usager dans la liste du gestionnaire
*
*param Usager* usager : le pointeur vers l'usager a supprimer
*******************************************/
void Gestionnaire::supprimerUsager(Usager* usager)
{
    auto it = find(usagers_.begin(), usagers_.end(), usager);

    if (it != usagers_.end()) {
        Usager* u = *it;
        usagers_.erase(it);
        emit usagerSupprime(u);
    }
}

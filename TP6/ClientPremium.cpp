/********************************************
* Titre: Travail pratique #6 - ClientPremium.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "ClientPremium.h"
#include <iostream>

/********************************************
*Le premier constructeur par param�tres de la classe ClientPremium initialise les attributs h�riter d'usager � leur
*valeur par default et le nombre de jour restant � la valeur passer passer en param�tre
*******************************************/
ClientPremium::ClientPremium(unsigned int joursRestants)
    : Client(),
      joursRestants_(joursRestants)
{
}

/********************************************
*Le deuxieme constructeur par param�tres de la ClientPremium initialise les attributs aux valeurs passer en param�tre
*******************************************/
ClientPremium::ClientPremium(const string &nom, const string &prenom, int identifiant,
                             const string &codePostal,
                             unsigned int joursRestants)
    : Client(nom, prenom, identifiant, codePostal),
      joursRestants_(joursRestants)
{
}

/********************************************
*La fonction obtenirJoursRestants retourne la valeur du parametre joursRestants_
*
*return : Retourne le nombre de jour restant au status premium du client
*******************************************/
unsigned int ClientPremium::obtenirJoursRestants() const
{
    return joursRestants_;
}

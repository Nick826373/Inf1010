/********************************************
* Titre: Travail pratique #6 - Solde.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Solde.h"

/********************************************
*Le constructeur par param�tres de la Solde initialise les attributs aux valeurs passer en param�tre
*Puisque la classe solde est abstraite le constructeur est uniquement appeler par les classe enfant.
*******************************************/
Solde::Solde(int pourcentageRabais)
    : pourcentageRabais_(pourcentageRabais)
{
}

/********************************************
*Le constructeur de la class Solde 
*******************************************/
Solde::~Solde() {}

/********************************************
*La fonction obtenirPourcentageRabais retourne la valeur du parametre pourcentageRabais_
*
*return : Retourne le pourcentage de rabais � appliquer sur le produit.
*******************************************/
double Solde::obtenirPourcentageRabais() const
{
    return pourcentageRabais_;
}

/********************************************
*La fonction modifierPourcentageRabais modifie le parametre pourcentageRabais_
*
*param pourcentageRabais : le nouveau pourcentae de rabais.
*******************************************/
void Solde::modifierPourcentageRabais(int pourcentageRabais)
{
    pourcentageRabais_ = pourcentageRabais;
}
/********************************************
* Titre: Travail pratique #6 - Client.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Client.h"
#include <iostream>

/********************************************
*Le constructeur par default de la classe Client initialise les attributs h�riter d'usager � leur
*valeur par default
*******************************************/
Client::Client()
    : Usager()
{
}

/********************************************
*Le constructeur par param�tres de la Fournisseur initialise les attributs aux valeurs passer en param�tre
*******************************************/
Client::Client(const string &nom, const string &prenom, int identifiant,
               const string &codePostal)
    : Usager(nom, prenom, identifiant, codePostal)
{
}

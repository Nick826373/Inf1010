/********************************************
* Titre: Travail pratique #6 - Produit.cpp
* Date: 19 avril 2018
* Auteur: Nicolas Bouchard (1855776) et Kamgain-Djoko Kuchue (1894656)
*******************************************/
#include "Produit.h"
#include "Fournisseur.h"
#include <iostream>
#include <string>

/********************************************
*Le constructeur par param�tres de la classe Produit initialise les attributs aux valeurs passer en param�tre ou aux valeurs par default
*Le produit est ajouter au catalogue de son fournisseur si un fournisseur est passer en param�tre
*******************************************/
Produit::Produit(Fournisseur *fournisseur, const string &nom, int reference, double prix)
    : fournisseur_(fournisseur),
      nom_(nom),
      reference_(reference),
      prix_(prix)
{
    if (fournisseur_ != nullptr)
        fournisseur_->ajouterProduit(this);
}

/********************************************
*La fonction obtenirNom retourne la valeur du parametre nom_
*
*return : Retourne le nom du produit.
*******************************************/
string Produit::obtenirNom() const
{
	return nom_;
}

/********************************************
*La fonction obtenirReference retourne la valeur du parametre reference_
*
*return : Retourne le numero reference du produit.
*******************************************/
int Produit::obtenirReference() const
{
	return reference_;
}

/********************************************
*La fonction obtenirPrix retourne la valeur du parametre prix_
*
*return : Retourne le prix du produit.
*******************************************/
double Produit::obtenirPrix() const
{
	return prix_;
}

/********************************************
*La fonction obtenirFournisseur retourne la valeur du parametre fournisseur_
*
*return : Retourne un pointeur vers le fournisseur du produit.
*******************************************/
Fournisseur *Produit::obtenirFournisseur() const
{
	return fournisseur_;
}

/********************************************
*La fonction afficher permet d'afficher les attributs d'un produit facilement dans la console.
*******************************************/
void Produit::afficher() const
{
    cout << "\t" << nom_ << endl
         << "\t\treference:\t" << reference_ << endl
         << "\t\tprix:\t\t" << "$" << prix_ << endl;
    if (fournisseur_ != nullptr)
        cout << "\t\tfournisseur:\t" << fournisseur_->obtenirNom() << endl;
}

/********************************************
*La fonction modifierNom modifie le parametre nom_
*
*param nom : nom_.
*******************************************/
void Produit::modifierNom(const string &nom)
{
    nom_ = nom;
}

/********************************************
*La fonction modifierReference modifie le parametre reference_
*
*param reference : reference_.
*******************************************/
void Produit::modifierReference(int reference)
{
    reference_ = reference;
}

/********************************************
*La fonction modifierPrix modifie le parametre prix_
*
*param prix : prix_.
*******************************************/
void Produit::modifierPrix(double prix)
{
	prix_ = prix;
}

/********************************************
*La fonction modifierPrix modifie le parametre fournisseur_
*
*param fournisseur : fournisseur_.
*******************************************/
void Produit::modifierFournisseur(Fournisseur *fournisseur)
{
	fournisseur_ = fournisseur;
}
